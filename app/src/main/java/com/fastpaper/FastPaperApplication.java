package com.fastpaper;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.widget.Toast;

import com.fastpaper.api.ApiClient;
import com.fastpaper.api.ApiInterface;
import com.fastpaper.model.GeneralModel;
import com.fastpaper.model.TypesModel;
import com.fastpaper.util.Constants;
import com.fastpaper.util.Preferences;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FastPaperApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        Intercom.initialize(this, "android_sdk-f4ad7bd91f49e21c68d3418d22a49e7234d78daf", "m0rx37s9");

        Registration registration = Registration.create();
        if (Preferences.getUserData(this) != null) {
            registration.withEmail(Preferences.getUserData(this).email);
            registration.withUserId(Preferences.getUserData(this).userId);
            Intercom.client().registerIdentifiedUser(registration);
        } else {
            Intercom.client().registerUnidentifiedUser();
        }

        //testMethod();
        getTypes(Constants.PAPER_TYPES);
        getTypes(Constants.DISCIPLIN_TYPES);
        getTypes(Constants.FORMATED_STYLE_TYPES);
        getTypes(Constants.SUBJECTS_TYPES);
        getTypes(Constants.CATEGORY_TYPES);

        TypesModel typesModel = new TypesModel();
        List<TypesModel.Data> list = new ArrayList<>();
        TypesModel.Data data = new TypesModel.Data();
        data.id = "4";
        data.acctId = "2";
        data.academicLevelName = getString(R.string.any_writer);
        data.academicLevelId = "any_writer";
        list.add(data);

        data = new TypesModel.Data();
        data.id = "4";
        data.acctId = "2";
        data.academicLevelName = getString(R.string.top_10_writer);
        data.academicLevelId = "top_10_writer";
        list.add(data);

        data = new TypesModel.Data();
        data.id = "4";
        data.acctId = "2";
        data.academicLevelName = getString(R.string.my_old_writer);
        data.academicLevelId = "my_previous_writer";
        list.add(data);

        typesModel.data = list;

        Preferences.saveTypes(getApplicationContext(), typesModel.data, Constants.ACADEMIC_TYPES);
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null && cm.getActiveNetworkInfo() != null) {
            return true;
        }
        Toast.makeText(this, "connect to internet", Toast.LENGTH_SHORT).show();
        return false;
    }

    public ApiInterface getService() {
        return ApiClient.getClient().create(ApiInterface.class);
    }

    public void getTypes(final String types) {
        if (!isNetworkConnected())
            return;

        Call<TypesModel> call = getService().getType(types);
        call.enqueue(new Callback<TypesModel>() {
            @Override
            public void onResponse(Call<TypesModel> call, Response<TypesModel> response) {
                TypesModel typesModel = response.body();
                if (typesModel != null) {
                    if (checkStatus(typesModel)) {
                        Log.e("Response ==>  ", types);
                        Preferences.saveTypes(getApplicationContext(), typesModel.data, types);
                    }
                }
            }

            @Override
            public void onFailure(Call<TypesModel> call, Throwable t) {
                //Toast.makeText(FastPaperApplication.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void testMethod() {
        if (!isNetworkConnected())
            return;

        Call<Object> call = getService().testMethod("android_subtest");
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String json = new Gson().toJson(response.body());
                Log.e("Test Response : ", json);
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                //Toast.makeText(FastPaperApplication.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean checkStatus(GeneralModel model) {
        if (model.success != null) {
            switch (model.success) {
                case "1":
                    return true;
            }
        } else if (model.flag != null) {
            return model.flag.equals("1");
        }
        return false;
    }
}
