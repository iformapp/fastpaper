package com.fastpaper.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.fastpaper.R;
import com.fastpaper.model.GeneralModel;
import com.fastpaper.model.OrderModel;
import com.fastpaper.ui.BaseActivity;
import com.fastpaper.ui.order.OrderActivity;
import com.fastpaper.ui.order.OrderDetailsActivity;
import com.fastpaper.util.Constants;
import com.fastpaper.util.Utils;
import com.fastpaper.util.textview.TextViewSFDisplayBold;
import com.fastpaper.util.textview.TextViewSFDisplayRegular;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderAdapter extends RecyclerSwipeAdapter<OrderAdapter.SimpleViewHolder> {

    private Context mContext;
    private List<OrderModel.Data> mDataset;
    private EditOrderListner editOrderListner;

    public OrderAdapter(Context context) {
        this.mContext = context;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_items, parent, false);
        return new SimpleViewHolder(view);
    }

    public void doRefresh(List<OrderModel.Data> objects) {
        this.mDataset = objects;
        notifyDataSetChanged();
    }

    public interface EditOrderListner {
        public void onEditOrder();
    }

    public void setEditOrderListner(EditOrderListner editOrderListner) {
        this.editOrderListner = editOrderListner;
    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder viewHolder, final int position) {
        final OrderModel.Data item = mDataset.get(position);
        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);

        viewHolder.rlItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, OrderDetailsActivity.class);
                i.putExtra(Constants.ORDER_DATA, item);
                mContext.startActivity(i);
                ((BaseActivity) mContext).openToLeft();
            }
        });

        viewHolder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDeleteDialog(item.orderId, viewHolder.swipeLayout, position);
            }
        });

        viewHolder.llEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editOrderListner != null) {
                    editOrderListner.onEditOrder();
                }
            }
        });
        viewHolder.tvOrderid.setText(item.orderId);
        viewHolder.tvDate.setText(Utils.changeDateFormat("MM-dd-yyyy hh:mm:ss", "MMM d, yyyy", item.orderDate));
        viewHolder.tvPrice.setText(item.total);
        if (item.paymentStatus.toLowerCase().equalsIgnoreCase("unpaid")) {
            viewHolder.dividerLine.setBackgroundColor(ContextCompat.getColor(mContext, R.color.reset_pw_btn));
            viewHolder.tvStatusWaiting.setVisibility(View.VISIBLE);
            viewHolder.tvStatus.setVisibility(View.GONE);
            viewHolder.tvStatusWaiting.setText(item.orderStatusName);
        } else {
            viewHolder.tvStatusWaiting.setVisibility(View.GONE);
            viewHolder.tvStatus.setVisibility(View.VISIBLE);
            if (item.orderStatusName.equalsIgnoreCase("Processing")) {
                viewHolder.dividerLine.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                viewHolder.tvStatus.setText(item.days + " days: " + item.hours + " h: " + item.minutes + " m: " + item.seconds + " s");
            } else if (item.orderStatusName.equalsIgnoreCase("Completed")) {
                viewHolder.dividerLine.setBackgroundColor(ContextCompat.getColor(mContext, R.color.lightgreen));
                viewHolder.tvStatus.setText(item.orderStatusName);
            }
        }
        mItemManger.bindView(viewHolder.itemView, position);
        viewHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onClose(SwipeLayout layout) {
                ((OrderActivity) mContext).setToggleDeleteOff();
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset != null ? mDataset.size() : 0;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rl_itemview)
        RelativeLayout rlItemView;
        @BindView(R.id.ll_edit)
        LinearLayout llEdit;
        @BindView(R.id.ll_delete)
        LinearLayout llDelete;
        @BindView(R.id.divider_line)
        View dividerLine;
        @BindView(R.id.tv_orderid)
        TextViewSFDisplayBold tvOrderid;
        @BindView(R.id.tv_date)
        TextViewSFDisplayRegular tvDate;
        @BindView(R.id.tv_status)
        TextViewSFDisplayRegular tvStatus;
        @BindView(R.id.tv_price)
        TextViewSFDisplayBold tvPrice;
        @BindView(R.id.swipe)
        SwipeLayout swipeLayout;
        @BindView(R.id.tv_status_waiting)
        TextViewSFDisplayRegular tvStatusWaiting;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void showDeleteDialog(final String orderId, final SwipeLayout swipeLayout, final int position) {
        final Dialog dialog = new Dialog(mContext, R.style.Theme_AppCompat_Dialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_edit_order);
        dialog.setCancelable(true);

        TextView tvMessage = (TextView) dialog.findViewById(R.id.tv_message);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        TextView tvChatnow = (TextView) dialog.findViewById(R.id.tv_chat_now);

        tvMessage.setText(Utils.fromHtml(String.format(mContext.getString(R.string.delete_order_text), orderId)));

        tvCancel.setText(mContext.getString(R.string.no));
        tvChatnow.setText(mContext.getString(R.string.yes));
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvChatnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                deleteOrders(orderId, swipeLayout, position);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    private void deleteOrders(String orderId, final SwipeLayout swipeLayout, final int position) {
        final BaseActivity activity = (BaseActivity) mContext;
        if (activity == null || !activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<GeneralModel> call = activity.getService().deleteOrder(activity.getAccessToken(), activity.getUserId(), orderId);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (activity.checkStatus(response.body())) {
                        mItemManger.removeShownLayouts(swipeLayout);
                        mDataset.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, mDataset.size());
                        mItemManger.closeAllItems();
                        Toast.makeText(activity, "Deleted", Toast.LENGTH_SHORT).show();
                    }
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                activity.failureError("delete Order failed");
            }
        });
    }
}
