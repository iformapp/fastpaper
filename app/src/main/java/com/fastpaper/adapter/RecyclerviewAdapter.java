package com.fastpaper.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class RecyclerviewAdapter extends RecyclerView.Adapter<RecyclerviewAdapter.SimpleViewHolder> {

    private ArrayList<?> mDataset;
    private int layoutId;
    private OnViewBindListner onViewBindListner;

    public RecyclerviewAdapter(ArrayList<?> objects, int layoutId, OnViewBindListner onViewBindListner) {
        this.mDataset = objects;
        this.layoutId = layoutId;
        this.onViewBindListner = onViewBindListner;
    }

    public interface OnViewBindListner {
        public void bindView(View view, int position);
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {
        if (onViewBindListner != null) {
            onViewBindListner.bindView(viewHolder.itemView, position);
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        public SimpleViewHolder(View itemView) {
            super(itemView);
        }
    }
}
