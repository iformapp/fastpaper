package com.fastpaper.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import com.fastpaper.R;
import com.fastpaper.model.TypesModel.Data;
import com.fastpaper.util.Constants;
import com.fastpaper.util.textview.TextViewSFTextBold;
import com.fastpaper.util.textview.TextViewSFTextRegular;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TypesAdapter extends RecyclerView.Adapter<TypesAdapter.SimpleViewHolder>
        implements Filterable {

    private List<Data> mDataset;
    private List<Data> mDatasetFiltered;
    private Context context;
    private boolean isSearchMode = false;
    private String types;

    public TypesAdapter(Context context, List<Data> objects, String types) {
        this.mDataset = objects;
        this.mDatasetFiltered = objects;
        this.context = context;
        this.types = types;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_types, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder holder, final int position) {
        Data item = mDatasetFiltered.get(position);
        String titleText;
        switch (types) {
            case Constants.PAPER_TYPES:
                titleText = item.paperName;
                break;
            case Constants.ACADEMIC_TYPES:
                titleText = item.academicLevelName;
                break;
            case Constants.SUBJECTS_TYPES:
                titleText = item.subjectName;
                break;
            case Constants.DISCIPLIN_TYPES:
                titleText = item.discipline;
                break;
            case Constants.FORMATED_STYLE_TYPES:
                titleText = item.styleName;
                break;
            case Constants.CATEGORY_TYPES:
                titleText = item.category;
                break;
            default:
                titleText = "";
                break;
        }
        holder.tvTitle.setText(titleText);
        if (isSearchMode) {
            if (position == 0) {
                holder.tvHeading.setVisibility(View.VISIBLE);
                holder.tvHeading.setText(R.string.all_types);
            } else {
                holder.tvHeading.setVisibility(View.GONE);
            }
        } else {
            if (position == 0) {
                holder.tvHeading.setVisibility(View.VISIBLE);
                holder.tvHeading.setText(R.string.most_popular);
            } else if (position == 6) {
                holder.tvHeading.setVisibility(View.VISIBLE);
                holder.tvHeading.setText(R.string.all_types);
            } else {
                holder.tvHeading.setVisibility(View.GONE);
            }
        }

        if (item.isSelected) {
            holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            Typeface tf = Typeface.createFromAsset(context.getAssets(), Constants.SFTEXT_BOLD);
            holder.tvTitle.setTypeface(tf);
            holder.imgChecked.setVisibility(View.VISIBLE);
        } else {
            holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.black));
            Typeface tf = Typeface.createFromAsset(context.getAssets(), Constants.SFTEXT_REGULAR);
            holder.tvTitle.setTypeface(tf);
            holder.imgChecked.setVisibility(View.GONE);
        }

        holder.rlView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSelected();
                mDatasetFiltered.get(position).isSelected = true;
                notifyDataSetChanged();
            }
        });
    }

    private void clearSelected() {
        if (mDatasetFiltered != null && mDatasetFiltered.size() > 0) {
            for (int i = 0; i < mDatasetFiltered.size(); i++) {
                mDatasetFiltered.get(i).isSelected = false;
            }
        }
    }

    private void selectFirst() {
        if (mDatasetFiltered != null && mDatasetFiltered.size() > 0) {
            mDatasetFiltered.get(0).isSelected = true;
        }
    }

    public Data getSelectedItem() {
        if (mDatasetFiltered != null && mDatasetFiltered.size() > 0) {
            for (int i = 0; i < mDatasetFiltered.size(); i++) {
                if (mDatasetFiltered.get(i).isSelected) {
                    return mDatasetFiltered.get(i);
                }
            }
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return mDatasetFiltered != null ? mDatasetFiltered.size() : 0;
    }

    public List<Data> getData() {
        return mDatasetFiltered;
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_heading)
        TextViewSFTextBold tvHeading;
        @BindView(R.id.tv_title)
        TextViewSFTextRegular tvTitle;
        @BindView(R.id.img_checked)
        ImageView imgChecked;
        @BindView(R.id.rl_view)
        View rlView;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    isSearchMode = false;
                    mDatasetFiltered = mDataset;
                } else {
                    isSearchMode = true;
                    List<Data> filteredList = new ArrayList<>();
                    for (Data row : mDataset) {
                        String rowText = "";
                        switch (types) {
                            case Constants.PAPER_TYPES:
                                rowText = row.paperName.toLowerCase();
                                break;
                            case Constants.ACADEMIC_TYPES:
                                rowText = row.academicLevelName.toLowerCase();
                                break;
                            case Constants.SUBJECTS_TYPES:
                                rowText = row.subjectName.toLowerCase();
                                break;
                            case Constants.DISCIPLIN_TYPES:
                                rowText = row.discipline.toLowerCase();
                                break;
                            case Constants.FORMATED_STYLE_TYPES:
                                rowText = row.styleName.toLowerCase();
                                break;
                            case Constants.CATEGORY_TYPES:
                                rowText = row.category.toLowerCase();
                                break;
                        }
                        if (!TextUtils.isEmpty(rowText)) {
                            if (rowText.contains(charString.toLowerCase())) {
                                filteredList.add(row);
                            }
                        }
                    }

                    mDatasetFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mDatasetFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mDatasetFiltered = (List<Data>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
