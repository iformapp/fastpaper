package com.fastpaper.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.fastpaper.R;
import com.fastpaper.model.FileUpload;
import com.fastpaper.ui.BaseActivity;
import com.fastpaper.ui.neworder.NewOrderActivity;
import com.fastpaper.util.Constants;
import com.fastpaper.util.textview.TextViewSFDisplayRegular;
import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadFileAdapter extends RecyclerView.Adapter<UploadFileAdapter.SimpleViewHolder> {

    private List<FileUpload.Data> mDataset;
    private Context context;
    private BaseActivity activity;

    public UploadFileAdapter(Context context) {
        this.context = context;
        activity = ((BaseActivity) context);
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_uploaded_files, parent, false);
        return new SimpleViewHolder(view);
    }

    public void doRefresh(List<FileUpload.Data> mDataset) {
        this.mDataset = mDataset;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {
        viewHolder.tvFileName.setText(mDataset.get(position).filePath);

        viewHolder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteFile(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_file_name)
        TextViewSFDisplayRegular tvFileName;
        @BindView(R.id.img_delete)
        ImageView imgDelete;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void deleteFile(final int position) {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Log.e("Delete File Url = > ", Constants.BASE_URL + Constants.DELETE_UPLOAD);
        Log.e("Params => ", "filename : " + mDataset.get(position).filePath);

        Call<FileUpload> call = activity.getService().deleteFile(mDataset.get(position).filePath);
        call.enqueue(new Callback<FileUpload>() {
            @Override
            public void onResponse(Call<FileUpload> call, Response<FileUpload> response) {
                mDataset.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, mDataset.size());
                ((NewOrderActivity) context).setFileAdapter(mDataset);
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<FileUpload> call, Throwable t) {
                activity.failureError("delete file failed");
            }
        });
    }
}
