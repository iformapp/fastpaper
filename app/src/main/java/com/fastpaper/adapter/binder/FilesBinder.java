package com.fastpaper.adapter.binder;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ahamed.multiviewadapter.BaseViewHolder;
import com.ahamed.multiviewadapter.ItemBinder;
import com.fastpaper.R;
import com.fastpaper.model.UserFiles;
import com.fastpaper.ui.BaseActivity;
import com.fastpaper.util.Constants;
import com.fastpaper.util.Utils;
import com.fastpaper.util.textview.TextViewSFDisplayRegular;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.ThinDownloadManager;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilesBinder extends ItemBinder<UserFiles, FilesBinder.FilesViewHolder> {

    private Context mContext;

    @Override
    public FilesViewHolder create(LayoutInflater inflater, ViewGroup parent) {
        mContext = parent.getContext();
        return new FilesViewHolder(inflater.inflate(R.layout.item_list_files, parent, false));
    }

    @Override
    public boolean canBindData(Object item) {
        return item instanceof UserFiles;
    }

    @Override
    public void bind(FilesViewHolder holder, final UserFiles item) {
        holder.tvFileName.setText(item.filepath);
        holder.tvDate.setText(Utils.changeDateFormat("MM-dd-yyyy", "dd/MM/yyyy", item.fileUploadSimpleDate));
        holder.tvOwner.setText("By me");
        if (holder.tvOwner.getText().toString().equals("By me")) {
            holder.imgView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.eye_gray));
        } else {
            holder.imgView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.menu_dot));
        }

        holder.imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOptionDialog(item);
            }
        });
    }

    static class FilesViewHolder extends BaseViewHolder<UserFiles> {

        @BindView(R.id.tv_file_name)
        TextViewSFDisplayRegular tvFileName;
        @BindView(R.id.tv_date)
        TextViewSFDisplayRegular tvDate;
        @BindView(R.id.tv_owner)
        TextViewSFDisplayRegular tvOwner;
        @BindView(R.id.img_view)
        ImageView imgView;

        public FilesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void showOptionDialog(UserFiles userFiles) {
        final Dialog dialog = new Dialog(mContext, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_file_option_menu);
        dialog.setCancelable(true);

        View llDownload = dialog.findViewById(R.id.ll_download);
        View llEmail = dialog.findViewById(R.id.ll_email);
        View llShare = dialog.findViewById(R.id.ll_share);
        TextView btnCancel = dialog.findViewById(R.id.btn_cancel);

        final String url = Constants.FILE_PATH + userFiles.filepath;

        llDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(url, true, false, false);
            }
        });

        llShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(url, false, false, true);
            }
        });

        llEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(url, false, true, false);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    private void downloadFile(final String filepath, final boolean isDownload, final boolean isEmailShare, boolean isShare) {
        String filename = filepath.substring(filepath.lastIndexOf("/") + 1);
        File folder = new File(Environment.getExternalStorageDirectory(), "/Download/FastPaper");
        if (!folder.exists())
            folder.mkdir();

        final File file = new File(folder.getAbsolutePath() + "/" + filename);
        if (!file.exists()) {
            ((BaseActivity) mContext).showProgress();

            ThinDownloadManager downloadManager = new ThinDownloadManager();
            Uri downloadUri = Uri.parse(filepath);
            Uri destinationUri = Uri.parse(file.getAbsolutePath());
            DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                    .setRetryPolicy(new DefaultRetryPolicy())
                    .setDestinationURI(destinationUri)
                    .setPriority(DownloadRequest.Priority.HIGH)
                    .setStatusListener(new DownloadStatusListenerV1() {
                        @Override
                        public void onDownloadComplete(DownloadRequest downloadRequest) {
                            ((BaseActivity) mContext).hideProgress();
                            if (!isDownload) {
                                shareFile(file, isEmailShare);
                            } else {
                                Toast.makeText(mContext, "download complete", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage) {
                            ((BaseActivity) mContext).hideProgress();
                            Toast.makeText(mContext, "download failed", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress) {

                        }
                    });
            downloadManager.add(downloadRequest);
        } else {
            if (!isDownload) {
                shareFile(file, isEmailShare);
            } else {
                Toast.makeText(mContext, "Already Downloaded", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void shareFile(File file, boolean isEmailShare) {
        String absolutePath = file.getAbsolutePath();
        Intent intentShareFile = new Intent(isEmailShare ? Intent.ACTION_SENDTO : Intent.ACTION_SEND);
        intentShareFile.setType("audio/*|image/*|application/pdf");
        if (isEmailShare) {
            intentShareFile.setData(Uri.parse("mailto:"));
            intentShareFile.putExtra(Intent.EXTRA_EMAIL, "");
        }
        intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + absolutePath));
        mContext.startActivity(Intent.createChooser(intentShareFile, "Share File"));
    }

    private void checkPermission(final String filepath, final boolean isDownload, final boolean isEmailShare, final boolean isShare) {
        Dexter.withActivity((Activity) mContext)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        downloadFile(filepath, isDownload, isEmailShare, isShare);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        Toast.makeText(mContext, "give storage permission first", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }
}