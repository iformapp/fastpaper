package com.fastpaper.adapter.binder;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ahamed.multiviewadapter.BaseViewHolder;
import com.ahamed.multiviewadapter.ItemBinder;
import com.fastpaper.R;
import com.fastpaper.model.OrderDetailsModel;
import com.fastpaper.util.textview.TextViewSFDisplayRegular;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderBinder extends ItemBinder<OrderDetailsModel, OrderBinder.OrderViewHolder> {

    private Context mContext;

    @Override
    public OrderViewHolder create(LayoutInflater inflater, ViewGroup parent) {
        mContext = parent.getContext();
        return new OrderViewHolder(inflater.inflate(R.layout.item_order_details, parent, false));
    }

    @Override
    public boolean canBindData(Object item) {
        return item instanceof OrderDetailsModel;
    }

    @Override
    public void bind(OrderViewHolder holder, OrderDetailsModel item) {
        holder.tvDescription.setText(item.description);
        holder.tvTitle.setText(item.title);
        if (!TextUtils.isEmpty(item.price)) {
            holder.tvTitle.setTextColor(Color.BLACK);
            holder.imgChecked.setVisibility(View.VISIBLE);
            holder.tvPrice.setText(item.price);
            holder.imgChecked.setImageDrawable(ContextCompat.getDrawable(mContext,
                    item.isSelect ? R.drawable.radio_selected : R.drawable.radio_unselect));
        } else {
            holder.tvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));
            holder.imgChecked.setVisibility(View.GONE);
        }
    }

    static class OrderViewHolder extends BaseViewHolder<OrderDetailsModel> {

        @BindView(R.id.tv_title)
        TextViewSFDisplayRegular tvTitle;
        @BindView(R.id.tv_price)
        TextViewSFDisplayRegular tvPrice;
        @BindView(R.id.tv_description)
        TextViewSFDisplayRegular tvDescription;
        @BindView(R.id.img_checked)
        ImageView imgChecked;

        public OrderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}