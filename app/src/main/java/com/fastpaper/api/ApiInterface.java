package com.fastpaper.api;

import com.fastpaper.model.CouponCode;
import com.fastpaper.model.FileUpload;
import com.fastpaper.model.GeneralModel;
import com.fastpaper.model.OrderByIdModel;
import com.fastpaper.model.OrderModel;
import com.fastpaper.model.PriceCalculate;
import com.fastpaper.model.SaveOrder;
import com.fastpaper.model.TypesModel;
import com.fastpaper.model.UserFiles;
import com.fastpaper.model.UserModel;
import com.fastpaper.util.Constants;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET(Constants.URL)
    Call<TypesModel> getType(@Query("method") String method);

    @GET(Constants.URL)
    Call<Object> testMethod(@Query("method") String method);

    @FormUrlEncoded
    @POST(Constants.LOGIN)
    Call<UserModel> login(@Field("user_email") String email, @Field("user_password") String password,
                          @Field("device_token") String token, @Field("device_type") String type,
                          @Field("timezone") String timezone, @Field("order_id") String orderId);

    @FormUrlEncoded
    @POST(Constants.REGISTER)
    Call<UserModel> register(@Field("email") String email, @Field("name") String name, @Field("password") String password,
                             @Field("dialCode") String dialCode, @Field("mobile") String mobile,
                             @Field("timezone") String timezone, @Field("countryname") String country);

    @FormUrlEncoded
    @POST(Constants.SAVE_PROFILE)
    Call<UserModel> saveProfile(@Field("accesstoken") String accesstoken, @Field("user_id") String userId,
                                @Field("first_name") String firstName, @Field("last_name") String last_name,
                                @Field("telephone") String telephone, @Field("telephone_prefix") String telephonePrefix);

    @FormUrlEncoded
    @POST(Constants.FORGOT_PASSWORD)
    Call<GeneralModel> forgotPassword(@Field("user_email") String email);

    @FormUrlEncoded
    @POST(Constants.SET_PASSWORD)
    Call<GeneralModel> setPassword(@Field("accesstoken") String accesstoken, @Field("user_password") String userPassword);

    @FormUrlEncoded
    @POST(Constants.SET_PASSWORD)
    Call<GeneralModel> resetPassword(@Field("accesstoken") String accesstoken, @Field("user_password") String userPassword,
                                     @Field("otp") String otp);

    @FormUrlEncoded
    @POST(Constants.SEND_FEEDBACK)
    Call<GeneralModel> sendFeedback(@Field("userid") String userid, @Field("message") String message);

    @FormUrlEncoded
    @POST(Constants.GET_ORDER)
    Call<OrderModel> getOrder(@Field("accesstoken") String accesstoken, @Field("user_id") String userId,
                              @Field("stat") String stat);

    @FormUrlEncoded
    @POST(Constants.DELETE_ORDER)
    Call<GeneralModel> deleteOrder(@Field("accesstoken") String accesstoken, @Field("user_id") String userId,
                                   @Field("order_id") String orderId);

    @FormUrlEncoded
    @POST(Constants.ORDER_DETAIL)
    Call<OrderByIdModel> getOrderById(@Field("accesstoken") String accesstoken, @Field("user_id") String userId,
                                      @Field("order_id") String orderId);

    @FormUrlEncoded
    @POST(Constants.ORDER_UPDATE)
    Call<GeneralModel> updateOrderStatus(@Field("payment_id") String paymentId, @Field("stat") String stat,
                                         @Field("order_id") String orderId);

    @FormUrlEncoded
    @POST(Constants.PRICE_CALCULATE)
    Call<PriceCalculate> getPrice(@Field("deadlineType") String deadlineType, @Field("deadlineValue") String deadlineValue,
                                  @Field("writerLevelId") int writerLevelId, @Field("page") String page,
                                  @Field("extraSendItToEmail") int extraSendItToEmail, @Field("extraAbstract") int extraAbstract,
                                  @Field("extraTurnintin") int extraTurnintin);

    @FormUrlEncoded
    @POST(Constants.PRICE_CALCULATE)
    Call<PriceCalculate> getPrice(@Field("deadlineType") String deadlineType, @Field("deadlineValue") String deadlineValue,
                                  @Field("writerLevelId") int writerLevelId, @Field("page") String page,
                                  @Field("extraSendItToEmail") int extraSendItToEmail, @Field("extraAbstract") int extraAbstract,
                                  @Field("extraTurnintin") int extraTurnintin, @Field("serviceTypeId") String serviceTypeId,
                                  @Field("paperTypeId") String paperTypeId, @Field("paperTypeOther") String paperTypeOther,
                                  @Field("subjectId") String subjectId, @Field("subjectOther") String subjectOther,
                                  @Field("chart") String chart, @Field("source") String source,
                                  @Field("slide") String slide, @Field("formatStyleId") String formatStyleId,
                                  @Field("formatStyleOther") String formatStyleOther, @Field("disciplineId") String disciplineId,
                                  @Field("writer") String writer, @Field("topic") String topic,
                                  @Field("spacing") String spacing, @Field("orderDetail") String orderDetail,
                                  @Field("writer_id") String writer_id);

    @FormUrlEncoded
    @POST(Constants.PRICE_CALCULATE)
    Call<Object> getPrice(@FieldMap Map<String, String> map);

    @Multipart
    @POST(Constants.FILE_UPLOAD)
    Call<FileUpload> uploadFile(@Part MultipartBody.Part file, @Part("category") RequestBody category,
                                @Part("order_id") RequestBody order_id, @Part("user_id") RequestBody user_id);

    @Multipart
    @POST(Constants.NEW_FILE_UPLOAD)
    Call<FileUpload> uploadNewFile(@Part MultipartBody.Part file, @Part("category") RequestBody category,
                               @Part("order_id") RequestBody order_id, @Part("user_id") RequestBody user_id,
                               @Part("accesstoken") RequestBody accesstoken);

    @FormUrlEncoded
    @POST(Constants.DELETE_UPLOAD)
    Call<FileUpload> deleteFile(@Field("filename") String filename);

    @GET(Constants.DELETE_ALL_FILES)
    Call<Object> deleteAllFiles();

    @FormUrlEncoded
    @POST(Constants.ORDER_SAVE)
    Call<SaveOrder> saveOrder(@Field("user_id") String user_id, @Field("accesstoken") String accesstoken,
                              @Field("order_id") String order_id);

    @FormUrlEncoded
    @POST(Constants.APPLY_COUPON)
    Call<CouponCode> applyCode(@Field("couponCode") String couponCode, @Field("order_id") String order_id,
                               @Field("accesstoken") String accesstoken);

    @FormUrlEncoded
    @POST(Constants.REMOVE_COUPON)
    Call<CouponCode> removeCode(@Field("order_id") String order_id, @Field("accesstoken") String accesstoken);
}
