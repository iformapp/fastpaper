package com.fastpaper.ccp;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.fastpaper.R;
import com.fastpaper.util.Constants;
import com.fastpaper.util.textview.TextViewSFTextBold;
import com.fastpaper.util.textview.TextViewSFTextRegular;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hbb20 on 11/1/16.
 */
class CountryCodeAdapter extends RecyclerView.Adapter<CountryCodeAdapter.CountryCodeViewHolder> {
    private List<CCPCountry> filteredCountries = null, masterCountries = null;
    private CountryCodePicker codePicker;
    private LayoutInflater inflater;
    private EditText editText_search;
    private Dialog dialog;
    private Context context;
    private boolean isSearchMode = false;
    private TextView tvApply;
    private TextView tvCancel;

    CountryCodeAdapter(Context context, List<CCPCountry> countries, CountryCodePicker codePicker, final EditText editText_search,
                       TextView tvApply, TextView tvCancel, Dialog dialog) {
        this.context = context;
        this.masterCountries = countries;
        this.codePicker = codePicker;
        this.dialog = dialog;
        this.tvApply = tvApply;
        this.tvCancel = tvCancel;
        this.editText_search = editText_search;
        this.inflater = LayoutInflater.from(context);
        this.filteredCountries = getFilteredCountries("");
        setSearchBar();
        setButtonClick();
    }

    private void setButtonClick() {
        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                codePicker.onUserTappedCountry(getSelectedItem());
                dialog.dismiss();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void setSearchBar() {
        if (codePicker.isSearchAllowed()) {
            setTextWatcher();
        }
    }

    /**
     * add textChangeListener, to apply new query each time editText get text changed.
     */
    private void setTextWatcher() {
        if (this.editText_search != null) {
            this.editText_search.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    applyQuery(s.toString());
                }
            });

            this.editText_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        InputMethodManager in = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        in.hideSoftInputFromWindow(editText_search.getWindowToken(), 0);
                        return true;
                    }

                    return false;
                }
            });
        }
    }

    /**
     * Filter country list for given keyWord / query.
     * Lists all countries that contains @param query in country's name, name code or phone code.
     *
     * @param query : text to match against country name, name code or phone code
     */
    private void applyQuery(String query) {
        query = query.toLowerCase();
        isSearchMode = !TextUtils.isEmpty(query);

        if (query.length() > 0 && query.charAt(0) == '+') {
            query = query.substring(1);
        }

        filteredCountries = getFilteredCountries(query);
        notifyDataSetChanged();
    }

    private List<CCPCountry> getFilteredCountries(String query) {
        List<CCPCountry> tempCCPCountryList = new ArrayList<CCPCountry>();
        if (codePicker.preferredCountries != null && codePicker.preferredCountries.size() > 0) {
            for (CCPCountry ccpCountry : codePicker.preferredCountries) {
                if (ccpCountry.isEligibleForQuery(query)) {
                    tempCCPCountryList.add(ccpCountry);
                }
            }

            if (tempCCPCountryList.size() > 0) { //means at least one preferred country is added.
                CCPCountry divider = null;
                tempCCPCountryList.add(divider);
            }
        }

        for (CCPCountry ccpCountry : masterCountries) {
            if (ccpCountry.isEligibleForQuery(query)) {
                tempCCPCountryList.add(ccpCountry);
            }
        }
        return tempCCPCountryList;
    }

    @Override
    public CountryCodeViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View rootView = inflater.inflate(R.layout.item_types, viewGroup, false);
        CountryCodeViewHolder viewHolder = new CountryCodeViewHolder(rootView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CountryCodeViewHolder holder, final int position) {
        CCPCountry ccpCountry = filteredCountries.get(position);
        if (ccpCountry != null) {
            holder.textView_name.setVisibility(View.VISIBLE);
            holder.textView_name.setText(ccpCountry.getName());
            if (isSearchMode) {
                if (position == 0) {
                    holder.tvHeading.setVisibility(View.VISIBLE);
                    holder.tvHeading.setText(R.string.all_types);
                } else {
                    holder.tvHeading.setVisibility(View.GONE);
                }
            } else {
                if (position == 0) {
                    holder.tvHeading.setVisibility(View.VISIBLE);
                    holder.tvHeading.setText(R.string.most_popular);
                } else if (position == 6) {
                    holder.tvHeading.setVisibility(View.VISIBLE);
                    holder.tvHeading.setText(R.string.all_types);
                } else {
                    holder.tvHeading.setVisibility(View.GONE);
                }
            }
            if (ccpCountry.isSelect()) {
                holder.textView_name.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                Typeface tf = Typeface.createFromAsset(context.getAssets(), Constants.SFTEXT_BOLD);
                holder.textView_name.setTypeface(tf);
                holder.imgChecked.setVisibility(View.VISIBLE);
            } else {
                holder.textView_name.setTextColor(ContextCompat.getColor(context, R.color.black));
                Typeface tf = Typeface.createFromAsset(context.getAssets(), Constants.SFTEXT_REGULAR);
                holder.textView_name.setTypeface(tf);
                holder.imgChecked.setVisibility(View.GONE);
            }

            holder.rlView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clearSelected();
                    filteredCountries.get(position).setSelect(true);
                    notifyDataSetChanged();
                }
            });
        }
    }

    public void clearSelected() {
        if (filteredCountries != null && filteredCountries.size() > 0) {
            for (int i = 0; i < filteredCountries.size(); i++) {
                filteredCountries.get(i).setSelect(false);
            }
        }
    }

    private CCPCountry getSelectedItem() {
        if (filteredCountries != null && filteredCountries.size() > 0) {
            for (int i = 0; i < filteredCountries.size(); i++) {
                if (filteredCountries.get(i).isSelect()) {
                    return filteredCountries.get(i);
                }
            }
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return filteredCountries.size();
    }

    class CountryCodeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_heading)
        TextViewSFTextBold tvHeading;
        @BindView(R.id.tv_title)
        TextViewSFTextRegular textView_name;
        @BindView(R.id.img_checked)
        ImageView imgChecked;
        @BindView(R.id.rl_view)
        View rlView;

        public CountryCodeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

