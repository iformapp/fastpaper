package com.fastpaper.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.fastpaper.R;
import com.fastpaper.ui.BaseActivity;
import com.fastpaper.util.textview.TextViewSFTextRegular;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BaseFragment extends Fragment {

    @Nullable
    @BindView(R.id.ll_view)
    LinearLayout llView;
    @Nullable
    @BindView(R.id.ll_download)
    LinearLayout llDownload;
    @Nullable
    @BindView(R.id.ll_email)
    LinearLayout llEmail;
    @Nullable
    @BindView(R.id.ll_share)
    LinearLayout llShare;
    @Nullable
    @BindView(R.id.btn_cancel)
    TextViewSFTextRegular btnCancel;

    private Dialog dialog;
    public BaseActivity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (BaseActivity) getActivity();
    }

    public void showOptionDialog(int position) {
        if (getActivity() == null) {
            return;
        }

        final Dialog dialog = new Dialog(getActivity(), R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_file_option_menu);
        dialog.setCancelable(true);
        ButterKnife.bind(this, dialog);

        if (llView != null) {
            llView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO :: put view code
                }
            });
        }

        if (llDownload != null) {
            llDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO :: put view code
                }
            });
        }

        if (llShare != null) {
            llShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO :: put view code
                }
            });
        }

        if (llEmail != null) {
            llEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO :: put view code
                }
            });
        }

        if (btnCancel != null) {
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //mainBinder.unbind();
            }
        });
    }
}
