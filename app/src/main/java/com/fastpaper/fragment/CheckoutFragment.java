package com.fastpaper.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.fastpaper.R;
import com.fastpaper.model.GeneralModel;
import com.fastpaper.model.OrderByIdModel;
import com.fastpaper.ui.neworder.CheckoutActivity;
import com.fastpaper.util.Constants;
import com.fastpaper.util.Utils;
import com.fastpaper.util.textview.TextViewSFDisplayBold;
import com.fastpaper.util.textview.TextViewSFDisplayRegular;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutFragment extends BaseFragment {

    @BindView(R.id.tv_order_no)
    TextViewSFDisplayBold tvOrderNo;
    @BindView(R.id.tv_deadline)
    TextViewSFDisplayRegular tvDeadline;
    @BindView(R.id.tv_subtotal)
    TextViewSFDisplayRegular tvSubtotal;
    @BindView(R.id.tv_discount)
    TextViewSFDisplayRegular tvDiscount;
    @BindView(R.id.tv_return)
    TextViewSFDisplayRegular tvReturn;
    @BindView(R.id.tv_redeem)
    TextViewSFDisplayRegular tvRedeem;
    @BindView(R.id.tv_total)
    TextViewSFDisplayBold tvTotal;
    @BindView(R.id.ll_checkout)
    LinearLayout llCheckout;

    private boolean isVisa;
    private OrderByIdModel.Data orderData;

    private static final String TAG = CheckoutFragment.class.getName();
    private static final int REQUEST_CODE_PAYMENT = 1;

    public static CheckoutFragment newInstanace(boolean isVisa) {
        CheckoutFragment fragment = new CheckoutFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.IS_VISA, isVisa);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_checkout, container, false);
        ButterKnife.bind(this, v);

        if (getArguments() != null) {
            isVisa = getArguments().getBoolean(Constants.IS_VISA);
        }

        if (getActivity() != null) {
            orderData = ((CheckoutActivity) getActivity()).getOrderData();
        }

        if (orderData != null) {
            tvOrderNo.setText(orderData.orderId);
            tvDeadline.setText(Utils.changeDateFormat("MM-dd-yyyy hh:mm:ss", "MM/dd/yyyy hh:mm a", orderData.orderDeadline));
            tvSubtotal.setText(Utils.priceWith$(orderData.subTotal));
            tvDiscount.setText(Utils.priceWith$(orderData.couponDiscount));
            tvRedeem.setText(Utils.priceWith$(orderData.redeem));
            tvTotal.setText(Utils.priceWith$(orderData.total));
        }
        return v;
    }

    @OnClick({R.id.tv_return, R.id.btn_checkout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_return:
                break;
            case R.id.btn_checkout:
                PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
                Intent intent = new Intent(getActivity(), PaymentActivity.class);
                intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, CheckoutActivity.config);
                intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
                startActivityForResult(intent, REQUEST_CODE_PAYMENT);
                break;
        }
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        PayPalItem payPalItem = new PayPalItem(orderData.orderId, 1, new BigDecimal(Utils.priceWithout$(orderData.total)),
                "USD", "Hip-0037");
        PayPalItem[] items = { payPalItem };
        return new PayPalPayment(new BigDecimal(Utils.priceWithout$(orderData.total)), "USD", "Fast Paper Payment", paymentIntent)
                .items(items);
    }

    protected void displayResultText(String result) {
        Toast.makeText(getActivity(), result, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.e(TAG, confirm.toJSONObject().toString(4));
                        Log.e(TAG, confirm.getPayment().toJSONObject().toString(4));
                        updateOrderStatus(confirm.toJSONObject().getJSONObject("response").getString("id"));
                    } catch (JSONException e) {
                        displayResultText("an extremely unlikely failure occurred: " + e);
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                displayResultText("The user canceled.");
                Log.e(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                displayResultText("An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
                Log.e(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    public void updateOrderStatus(String paymentId) {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<GeneralModel> call = activity.getService().updateOrderStatus(paymentId, "paid", orderData.orderId);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (activity.checkStatus(response.body())) {
                        Toast.makeText(activity, response.body().msg, Toast.LENGTH_SHORT).show();
                        activity.gotoMainActivity(Constants.TAB_ORDER);
                    }
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                activity.failureError("update password failed");
            }
        });
    }
}
