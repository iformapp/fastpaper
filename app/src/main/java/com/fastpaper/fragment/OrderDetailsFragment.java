package com.fastpaper.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.ahamed.multiviewadapter.SimpleRecyclerAdapter;
import com.fastpaper.R;
import com.fastpaper.adapter.binder.OrderBinder;
import com.fastpaper.model.OrderByIdModel;
import com.fastpaper.model.OrderDetailsModel;
import com.fastpaper.ui.order.OrderDetailsActivity;
import com.fastpaper.util.Utils;
import com.fastpaper.util.textview.TextViewSFDisplayRegular;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;

public class OrderDetailsFragment extends BaseFragment {

    @BindView(R.id.rv_order_details)
    RecyclerView rvOrderDetails;
    @BindView(R.id.rv_page_style)
    RecyclerView rvPageStyle;
    @BindView(R.id.rv_page_option)
    RecyclerView rvPageOption;
    @BindView(R.id.rv_order_type)
    RecyclerView rvOrderType;
    @BindView(R.id.tv_info)
    TextViewSFDisplayRegular tvInfo;

    private List<OrderDetailsModel> detailsModelList;
    private List<OrderDetailsModel> pageStyleList;
    private List<OrderDetailsModel> pageOptionList;
    private List<OrderDetailsModel> orderTypeList;
    private OrderByIdModel.Data orderData;

    public static OrderDetailsFragment newInstanace(boolean isVisa) {
        OrderDetailsFragment fragment = new OrderDetailsFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_order_details, container, false);
        ButterKnife.bind(this, v);

        if (getActivity() != null) {
            orderData = ((OrderDetailsActivity) getActivity()).getOrderData();
        }

        if (orderData != null) {
            detailsModelList = new ArrayList<>();
            pageStyleList = new ArrayList<>();
            pageOptionList = new ArrayList<>();
            orderTypeList = new ArrayList<>();

            SimpleRecyclerAdapter<OrderDetailsModel, OrderBinder> orderDetailsAdapter =
                    new SimpleRecyclerAdapter<>(new OrderBinder());

            rvOrderDetails.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvOrderDetails.setAdapter(orderDetailsAdapter);
            orderDetailsAdapter.setData(fillOrderList());

            SimpleRecyclerAdapter<OrderDetailsModel, OrderBinder> pageStyleAdapter =
                    new SimpleRecyclerAdapter<>(new OrderBinder());

            rvPageStyle.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvPageStyle.setAdapter(pageStyleAdapter);
            pageStyleAdapter.setData(fillPageStyleList());

            SimpleRecyclerAdapter<OrderDetailsModel, OrderBinder> pageOptionAdapter =
                    new SimpleRecyclerAdapter<>(new OrderBinder());

            rvPageOption.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvPageOption.setAdapter(pageOptionAdapter);
            pageOptionAdapter.setData(fillPageOptionList());

            SimpleRecyclerAdapter<OrderDetailsModel, OrderBinder> orderTypeAdapter =
                    new SimpleRecyclerAdapter<>(new OrderBinder());

            rvOrderType.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvOrderType.setAdapter(orderTypeAdapter);
            orderTypeAdapter.setData(fillOrderTypeList());

            rvPageOption.setNestedScrollingEnabled(false);
            rvPageStyle.setNestedScrollingEnabled(false);
            rvOrderDetails.setNestedScrollingEnabled(false);
            rvOrderType.setNestedScrollingEnabled(false);
        }
        return v;
    }

    public List fillOrderList() {
        detailsModelList.add(new OrderDetailsModel(getString(R.string.type_of_service), orderData.service, "", false));
        detailsModelList.add(new OrderDetailsModel(getString(R.string.writer_level), orderData.academic, "", false));
        detailsModelList.add(new OrderDetailsModel(getString(R.string.type_of_paper), orderData.paperName, "", false));
        detailsModelList.add(new OrderDetailsModel(getString(R.string.deadline), orderData.deadline, "", false));
        String pages = "";
        if (orderData.spacing.equalsIgnoreCase(getString(R.string.single_spaced))) {
            pages = "560 words";
        } else if (orderData.spacing.equalsIgnoreCase(getString(R.string.double_spaced))) {
            pages = "280 words";
        }
        detailsModelList.add(new OrderDetailsModel(getString(R.string.pages), orderData.pages + " / " + pages, "", false));
        detailsModelList.add(new OrderDetailsModel(getString(R.string.subject), orderData.subjectName, "", false));
        detailsModelList.add(new OrderDetailsModel(getString(R.string.topic), orderData.topic, "", false));
        return detailsModelList;
    }

    public List fillPageStyleList() {
        pageStyleList.add(new OrderDetailsModel(getString(R.string.preferred_writer), orderData.preferredWriter, "", false));
        pageStyleList.add(new OrderDetailsModel(getString(R.string.format_style), orderData.styleName, "", false));
        pageStyleList.add(new OrderDetailsModel(getString(R.string.discipline), orderData.discipline, "", false));
        return pageStyleList;
    }

    public List fillPageOptionList() {
        pageOptionList.add(new OrderDetailsModel(getString(R.string.turnitin_plagiarism_report), "", getString(R.string.turnitin_price),
                orderData.plagiarismReport.equalsIgnoreCase("yes")));
        pageOptionList.add(new OrderDetailsModel(getString(R.string.abstract_page), "", getString(R.string.abstract_price),
                orderData.abstructPage.equalsIgnoreCase("yes")));
        pageOptionList.add(new OrderDetailsModel(getString(R.string.send_it_to_my_e_mail), "", getString(R.string.send_email_price),
                orderData.isSendToMyEmail.equalsIgnoreCase("1")));
        return pageOptionList;
    }

    public List fillOrderTypeList() {
        orderTypeList.add(new OrderDetailsModel(getString(R.string.sorces), orderData.source, "", false));
        orderTypeList.add(new OrderDetailsModel(getString(R.string.charts), orderData.charts, "", false));
        orderTypeList.add(new OrderDetailsModel(getString(R.string.powerpoint_slide), orderData.powerpoint, "", false));
        return orderTypeList;
    }

    @OnClick(R.id.tv_edit)
    public void onViewClicked() {
        showEditDialog();
    }

    public void showEditDialog() {
        final Dialog dialog = new Dialog(activity, R.style.Theme_AppCompat_Dialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_edit_order);
        dialog.setCancelable(true);

        TextView tvMessage = (TextView) dialog.findViewById(R.id.tv_message);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        TextView tvChatnow = (TextView) dialog.findViewById(R.id.tv_chat_now);

        tvMessage.setText(Utils.fromHtml(getString(R.string.edit_order_text)));

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvChatnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intercom.client().displayMessenger();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }
}
