package com.fastpaper.fragment;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.fastpaper.R;
import com.fastpaper.model.CouponCode;
import com.fastpaper.model.OrderByIdModel;
import com.fastpaper.model.OrderModel;
import com.fastpaper.ui.BaseActivity;
import com.fastpaper.ui.neworder.CheckoutActivity;
import com.fastpaper.ui.order.OrderDetailsActivity;
import com.fastpaper.util.Constants;
import com.fastpaper.util.Utils;
import com.fastpaper.util.edittext.EditTextSFTextRegular;
import com.fastpaper.util.textview.TextViewSFDisplayBold;
import com.fastpaper.util.textview.TextViewSFDisplayRegular;
import com.fastpaper.util.textview.TextViewSFTextRegular;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderPaymentFragment extends BaseFragment {

    @BindView(R.id.tv_order_no)
    TextViewSFDisplayBold tvOrderNo;
    @BindView(R.id.tv_order_date)
    TextViewSFDisplayRegular tvOrderDate;
    @BindView(R.id.tv_deadline)
    TextViewSFDisplayRegular tvDeadline;
    @BindView(R.id.tv_paypal)
    TextViewSFDisplayRegular tvPaypal;
    @BindView(R.id.tv_sales_id)
    TextViewSFDisplayRegular tvSalesId;
    @BindView(R.id.tv_subtotal)
    TextViewSFDisplayRegular tvSubtotal;
    @BindView(R.id.label_coupon_code)
    TextViewSFDisplayRegular labelCouponCode;
    @BindView(R.id.tv_discount)
    TextViewSFDisplayRegular tvDiscount;
    @BindView(R.id.tv_redeem)
    TextViewSFDisplayRegular tvRedeem;
    @BindView(R.id.tv_total)
    TextViewSFDisplayBold tvTotal;
    @BindView(R.id.ll_payment_details)
    LinearLayout llPaymentDetails;
    @BindView(R.id.et_couponcode)
    EditTextSFTextRegular etCouponcode;
    @BindView(R.id.ll_coupon_code)
    LinearLayout llCouponCode;
    @BindView(R.id.tv_coupon_code)
    TextViewSFTextRegular tvCouponCode;
    @BindView(R.id.rl_applying_code)
    RelativeLayout rlApplyingCode;
    @BindView(R.id.tv_have_code)
    TextViewSFTextRegular tvHaveCode;
    @BindView(R.id.ll_checkout_pay)
    LinearLayout llCheckoutPay;
    @BindView(R.id.tv_pay_text)
    TextViewSFTextRegular tvPayText;
    @BindView(R.id.tv_pay_amount)
    TextViewSFDisplayRegular tvPayAmount;
    @BindView(R.id.tv_old_total)
    TextViewSFDisplayRegular tvOldTotal;

    private OrderByIdModel.Data orderData;
    private OrderModel.Data singleData;

    public static OrderPaymentFragment newInstanace(OrderModel.Data singleData) {
        OrderPaymentFragment fragment = new OrderPaymentFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.ORDER_DATA, singleData);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_order_payment, container, false);
        ButterKnife.bind(this, v);

        init();
        return v;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (orderData != null && singleData != null) {
                if (!orderData.status.equalsIgnoreCase("paid")) {
                    llPaymentDetails.setVisibility(View.GONE);
                    llCheckoutPay.setVisibility(View.VISIBLE);
                    tvPayText.setText(String.format(getString(R.string.payment_not_received), singleData.dead));
                    if (!TextUtils.isEmpty(orderData.couponCode)) {
                        tvHaveCode.setVisibility(View.GONE);
                        llCouponCode.setVisibility(View.GONE);
                        rlApplyingCode.setVisibility(View.VISIBLE);
                        tvOldTotal.setVisibility(View.VISIBLE);
                        tvCouponCode.setText(orderData.couponCode);
                        tvOldTotal.setText(orderData.subTotal);
                        tvPayAmount.setText(orderData.total);
                    } else {
                        tvPayAmount.setText(orderData.total);
                        tvOldTotal.setVisibility(View.GONE);
                        rlApplyingCode.setVisibility(View.GONE);
                        tvHaveCode.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    public void init() {
        if (activity != null) {
            orderData = ((OrderDetailsActivity) activity).getOrderData();
        }

        if (getArguments() != null) {
            singleData = (OrderModel.Data) getArguments().getSerializable(Constants.ORDER_DATA);
        }

        if (orderData != null && singleData != null) {
            if (orderData.status.equalsIgnoreCase("paid")) {
                llPaymentDetails.setVisibility(View.VISIBLE);
                llCheckoutPay.setVisibility(View.GONE);
                tvOrderNo.setText(orderData.orderId);
                tvOrderDate.setText(Utils.changeDateFormat("MM-dd-yyyy hh:mm:ss", "MM/dd/yyyy hh:mm a", orderData.orderDate));
                tvDeadline.setText(Utils.changeDateFormat("MM-dd-yyyy hh:mm:ss", "MM/dd/yyyy hh:mm a", orderData.orderDeadline));
                tvSalesId.setText(singleData.salesId);
                labelCouponCode.setText("");
                tvSubtotal.setText(orderData.subTotal);
                tvDiscount.setText("$" + orderData.couponDiscount);
                tvRedeem.setText(orderData.redeem);
                tvTotal.setText(orderData.total);
            } else {
                llPaymentDetails.setVisibility(View.GONE);
                llCheckoutPay.setVisibility(View.VISIBLE);
                tvPayText.setText(String.format(getString(R.string.payment_not_received), singleData.dead));
                tvPayAmount.setText(orderData.total);
                if (!TextUtils.isEmpty(orderData.couponCode)) {
                    tvHaveCode.setVisibility(View.GONE);
                    llCouponCode.setVisibility(View.GONE);
                    rlApplyingCode.setVisibility(View.VISIBLE);
                    tvOldTotal.setVisibility(View.VISIBLE);
                    tvCouponCode.setText(orderData.couponCode);
                    tvOldTotal.setText("$" + Utils.numberFormat(orderData.subTotal));
                    tvPayAmount.setText("$" + Utils.numberFormat(orderData.total));
                }
                tvOldTotal.setPaintFlags(tvOldTotal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
        }
    }

    @OnClick({R.id.tv_apply, R.id.img_coupon_close, R.id.tv_coupon_remove, R.id.rl_pay, R.id.tv_have_code})
    public void onViewClicked(View view) {
        Utils.hideSoftKeyboard(activity);
        switch (view.getId()) {
            case R.id.tv_apply:
                if (activity.isEmpty(etCouponcode.getText().toString())) {
                    Toast.makeText(activity, "enter coupon code", Toast.LENGTH_SHORT).show();
                    return;
                }
                applyCouponCode(etCouponcode.getText().toString());
                break;
            case R.id.img_coupon_close:
                tvHaveCode.setVisibility(View.VISIBLE);
                llCouponCode.setVisibility(View.GONE);
                etCouponcode.setText("");
                break;
            case R.id.tv_coupon_remove:
                removeCouponCode();
                break;
            case R.id.rl_pay:
                Intent i = new Intent(activity, CheckoutActivity.class);
                i.putExtra(Constants.ORDER_DATA, orderData);
                startActivity(i);
                activity.openToTop();
                break;
            case R.id.tv_have_code:
                tvHaveCode.setVisibility(View.GONE);
                llCouponCode.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void applyCouponCode(final String couponCode) {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<CouponCode> call = activity.getService().applyCode(couponCode, orderData.orderId, activity.getAccessToken());
        call.enqueue(new Callback<CouponCode>() {
            @Override
            public void onResponse(Call<CouponCode> call, Response<CouponCode> response) {
                CouponCode model = response.body();
                if (activity.checkStatus(model)) {
                    llCouponCode.setVisibility(View.GONE);
                    rlApplyingCode.setVisibility(View.VISIBLE);
                    tvOldTotal.setVisibility(View.VISIBLE);
                    tvCouponCode.setText(couponCode);
                    tvOldTotal.setText("$" + Utils.numberFormat(model.data.subTotal));
                    tvPayAmount.setText("$" + Utils.numberFormat(model.data.total));
                    orderData.total = model.data.total;
                    orderData.subTotal = model.data.subTotal;
                    orderData.couponDiscount = model.data.couponDiscount;
                    orderData.couponCode = couponCode;
                    ((OrderDetailsActivity) activity).setOrderData(orderData);
                } else {
                    if (!model.msg.equalsIgnoreCase(getString(R.string.invalid_access_token))) {
                        Toast.makeText(activity, model.msg, Toast.LENGTH_SHORT).show();
                    }
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<CouponCode> call, Throwable t) {
                activity.failureError("get code failed");
            }
        });
    }

    public void removeCouponCode() {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<CouponCode> call = activity.getService().removeCode(orderData.orderId, activity.getAccessToken());
        call.enqueue(new Callback<CouponCode>() {
            @Override
            public void onResponse(Call<CouponCode> call, Response<CouponCode> response) {
                CouponCode model = response.body();
                if (activity.checkStatus(model)) {
                    tvOldTotal.setVisibility(View.GONE);
                    tvPayAmount.setText("$" + Utils.numberFormat(model.data.total));
                    rlApplyingCode.setVisibility(View.GONE);
                    tvHaveCode.setVisibility(View.VISIBLE);
                    etCouponcode.setText("");
                    orderData.total = model.data.total;
                    orderData.subTotal = model.data.subTotal;
                    orderData.couponDiscount = model.data.couponDiscount;
                    orderData.couponCode = "";
                    ((OrderDetailsActivity) activity).setOrderData(orderData);
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<CouponCode> call, Throwable t) {
                activity.failureError("remove code failed");
            }
        });
    }
}
