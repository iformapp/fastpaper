package com.fastpaper.fragment;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.fastpaper.R;
import com.fastpaper.adapter.RecyclerviewAdapter;
import com.fastpaper.model.CouponCode;
import com.fastpaper.model.OrderByIdModel;
import com.fastpaper.model.OrderModel;
import com.fastpaper.ui.neworder.CheckoutActivity;
import com.fastpaper.ui.order.OrderDetailsActivity;
import com.fastpaper.util.Constants;
import com.fastpaper.util.Utils;
import com.fastpaper.util.edittext.EditTextSFTextRegular;
import com.fastpaper.util.textview.TextViewSFDisplayBold;
import com.fastpaper.util.textview.TextViewSFDisplayRegular;
import com.fastpaper.util.textview.TextViewSFTextRegular;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderStatusFragment extends BaseFragment implements RecyclerviewAdapter.OnViewBindListner {

    @Nullable
    @BindView(R.id.tv_days)
    TextViewSFDisplayBold tvDays;
    @Nullable
    @BindView(R.id.tv_hours)
    TextViewSFDisplayBold tvHours;
    @Nullable
    @BindView(R.id.tv_minutes)
    TextViewSFDisplayBold tvMinutes;
    @Nullable
    @BindView(R.id.tv_second)
    TextViewSFDisplayBold tvSecond;
    @Nullable
    @BindView(R.id.et_couponcode)
    EditTextSFTextRegular etCouponcode;
    @Nullable
    @BindView(R.id.ll_coupon_code)
    LinearLayout llCouponCode;
    @Nullable
    @BindView(R.id.tv_coupon_code)
    TextViewSFTextRegular tvCouponCode;
    @Nullable
    @BindView(R.id.rl_applying_code)
    RelativeLayout rlApplyingCode;
    @Nullable
    @BindView(R.id.tv_have_code)
    TextViewSFTextRegular tvHaveCode;
    @Nullable
    @BindView(R.id.tv_deadline_text)
    TextViewSFDisplayRegular tvDeadlineText;
    @Nullable
    @BindView(R.id.rl_waiting)
    RelativeLayout rlWaiting;
    @Nullable
    @BindView(R.id.ll_current)
    LinearLayout llCurrent;
    @Nullable
    @BindView(R.id.rv_files)
    RecyclerView rvFiles;
    @Nullable
    @BindView(R.id.ll_completed)
    LinearLayout llCompleted;
    @Nullable
    @BindView(R.id.tv_file_name)
    TextViewSFDisplayBold tvFileName;
    @Nullable
    @BindView(R.id.tv_date)
    TextViewSFDisplayRegular tvDate;
    @Nullable
    @BindView(R.id.tv_wr_id)
    TextViewSFDisplayBold tvWrId;
    @Nullable
    @BindView(R.id.img_menu)
    ImageView imgMenu;
    @Nullable
    @BindView(R.id.tv_pay_amount)
    TextViewSFDisplayRegular tvPayAmount;
    @Nullable
    @BindView(R.id.tv_old_total)
    TextViewSFDisplayRegular tvOldTotal;
    @Nullable
    @BindView(R.id.tv_days_p)
    TextViewSFDisplayBold tvDaysP;
    @Nullable
    @BindView(R.id.tv_hours_p)
    TextViewSFDisplayBold tvHoursP;
    @Nullable
    @BindView(R.id.tv_minutes_p)
    TextViewSFDisplayBold tvMinutesP;
    @Nullable
    @BindView(R.id.tv_second_p)
    TextViewSFDisplayBold tvSecondP;

    private RecyclerviewAdapter mAdapter;
    private OrderByIdModel.Data orderData;
    private OrderModel.Data singleData;

    public static OrderStatusFragment newInstanace(OrderModel.Data singleData) {
        OrderStatusFragment fragment = new OrderStatusFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.ORDER_DATA, singleData);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_order_status, container, false);
        ButterKnife.bind(this, v);

        if (activity != null) {
            orderData = ((OrderDetailsActivity) activity).getOrderData();
        }

        if (getArguments() != null) {
            singleData = (OrderModel.Data) getArguments().getSerializable(Constants.ORDER_DATA);
        }

        if (singleData != null) {
            if (singleData.paymentStatus.toLowerCase().equalsIgnoreCase("unpaid")) {
                if (rlWaiting != null) {
                    rlWaiting.setVisibility(View.VISIBLE);
                }
                if (llCompleted != null) {
                    llCompleted.setVisibility(View.GONE);
                }
                if (llCurrent != null) {
                    llCurrent.setVisibility(View.GONE);
                }
                if (tvDeadlineText != null) {
                    tvDeadlineText.setText(String.format(getString(R.string.deadline_text), singleData.dead));
                }
                if (tvPayAmount != null) {
                    tvPayAmount.setText(orderData.total);
                    if (!TextUtils.isEmpty(orderData.couponCode)) {
                        tvHaveCode.setVisibility(View.GONE);
                        llCouponCode.setVisibility(View.GONE);
                        rlApplyingCode.setVisibility(View.VISIBLE);
                        tvOldTotal.setVisibility(View.VISIBLE);
                        tvCouponCode.setText(orderData.couponCode);
                        tvOldTotal.setText(orderData.subTotal);
                        tvPayAmount.setText(orderData.total);
                    }
                }
                if (tvHours != null) {
                    tvHours.setText(orderData.hours + "");
                }
                if (tvDays != null) {
                    if (String.valueOf(orderData.days).length() > 3) {
                        tvDays.setText(String.valueOf(orderData.days).substring(0, 2) + "..");
                    } else {
                        tvDays.setText(orderData.days + "");
                    }
                }
                if (tvMinutes != null) {
                    tvMinutes.setText(orderData.minutes + "");
                }
                if (tvSecond != null) {
                    tvSecond.setText(orderData.seconds + "");
                }
            } else {
                if (singleData.orderStatusName.equalsIgnoreCase("Processing")) {
                    if (rlWaiting != null) {
                        rlWaiting.setVisibility(View.GONE);
                    }
                    if (llCompleted != null) {
                        llCompleted.setVisibility(View.GONE);
                    }
                    if (llCurrent != null) {
                        llCurrent.setVisibility(View.VISIBLE);
                    }
                    if (tvHoursP != null) {
                        tvHoursP.setText(orderData.hours + "");
                    }
                    if (tvDaysP != null) {
                        tvDaysP.setText(orderData.days + "");
                    }
                    if (tvMinutesP != null) {
                        tvMinutesP.setText(orderData.minutes + "");
                    }
                    if (tvSecondP != null) {
                        tvSecondP.setText(orderData.seconds + "");
                    }
                } else if (singleData.orderStatusName.equalsIgnoreCase("Completed")) {
                    if (rlWaiting != null) {
                        rlWaiting.setVisibility(View.GONE);
                    }
                    if (llCompleted != null) {
                        llCompleted.setVisibility(View.VISIBLE);
                    }
                    if (llCurrent != null) {
                        llCurrent.setVisibility(View.GONE);
                    }
                }
            }
            tvOldTotal.setPaintFlags(tvOldTotal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        if (rvFiles != null) {
            rvFiles.setLayoutManager(new LinearLayoutManager(activity));
            ArrayList<String> demoArray = new ArrayList<>();
            for (int i = 0; i < 3; i++) {
                demoArray.add(i + "");
            }
            mAdapter = new RecyclerviewAdapter(demoArray, R.layout.item_status_complete_files, this);
            rvFiles.setAdapter(mAdapter);
        }
        return v;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (orderData != null && singleData != null) {
                if (singleData.paymentStatus.equalsIgnoreCase("unpaid")) {
                    if (!TextUtils.isEmpty(orderData.couponCode)) {
                        tvHaveCode.setVisibility(View.GONE);
                        llCouponCode.setVisibility(View.GONE);
                        rlApplyingCode.setVisibility(View.VISIBLE);
                        tvOldTotal.setVisibility(View.VISIBLE);
                        tvCouponCode.setText(orderData.couponCode);
                        tvOldTotal.setText(orderData.subTotal);
                        tvPayAmount.setText(orderData.total);
                    } else {
                        tvPayAmount.setText(orderData.total);
                        tvOldTotal.setVisibility(View.GONE);
                        rlApplyingCode.setVisibility(View.GONE);
                        tvHaveCode.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    @Optional
    @OnClick({R.id.tv_waiting_payment, R.id.tv_not_paid, R.id.tv_apply, R.id.img_coupon_close, R.id.tv_coupon_remove, R.id.tv_have_code,
            R.id.tv_processing, R.id.tv_paid, R.id.tv_completed, R.id.tv_complete_paid, R.id.rl_pay})
    public void onViewClicked(View view) {
        Utils.hideSoftKeyboard(activity);
        switch (view.getId()) {
            case R.id.tv_waiting_payment:
                break;
            case R.id.tv_not_paid:
                break;
            case R.id.tv_apply:
                if (TextUtils.isEmpty(etCouponcode.getText().toString())) {
                    Toast.makeText(activity, "enter coupon code", Toast.LENGTH_SHORT).show();
                    return;
                }
                applyCouponCode(etCouponcode.getText().toString());
                break;
            case R.id.img_coupon_close:
                tvHaveCode.setVisibility(View.VISIBLE);
                llCouponCode.setVisibility(View.GONE);
                etCouponcode.setText("");
                break;
            case R.id.tv_coupon_remove:
                removeCouponCode();
                break;
            case R.id.rl_pay:
                Intent i = new Intent(activity, CheckoutActivity.class);
                i.putExtra(Constants.ORDER_DATA, orderData);
                startActivity(i);
                activity.openToTop();
                break;
            case R.id.tv_have_code:
                tvHaveCode.setVisibility(View.GONE);
                llCouponCode.setVisibility(View.VISIBLE);
                break;
            case R.id.tv_processing:
                break;
            case R.id.tv_paid:
                break;
            case R.id.tv_completed:
                Toast.makeText(activity, "Completed", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tv_complete_paid:
                break;
        }
    }

    public void applyCouponCode(final String couponCode) {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<CouponCode> call = activity.getService().applyCode(couponCode, orderData.orderId, activity.getAccessToken());
        call.enqueue(new Callback<CouponCode>() {
            @Override
            public void onResponse(Call<CouponCode> call, Response<CouponCode> response) {
                CouponCode model = response.body();
                if (activity.checkStatus(model)) {
                    llCouponCode.setVisibility(View.GONE);
                    rlApplyingCode.setVisibility(View.VISIBLE);
                    tvOldTotal.setVisibility(View.VISIBLE);
                    tvCouponCode.setText(couponCode);
                    tvOldTotal.setText("$" + Utils.numberFormat(model.data.subTotal));
                    tvPayAmount.setText("$" + Utils.numberFormat(model.data.total));
                    orderData.total = model.data.total;
                    orderData.subTotal = model.data.subTotal;
                    orderData.couponDiscount = model.data.couponDiscount;
                    orderData.couponCode = couponCode;
                    ((OrderDetailsActivity) activity).setOrderData(orderData);
                } else {
                    if (!model.msg.equalsIgnoreCase(getString(R.string.invalid_access_token))) {
                        Toast.makeText(activity, model.msg, Toast.LENGTH_SHORT).show();
                    }
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<CouponCode> call, Throwable t) {
                activity.failureError("get code failed");
            }
        });
    }

    public void removeCouponCode() {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<CouponCode> call = activity.getService().removeCode(orderData.orderId, activity.getAccessToken());
        call.enqueue(new Callback<CouponCode>() {
            @Override
            public void onResponse(Call<CouponCode> call, Response<CouponCode> response) {
                CouponCode model = response.body();
                if (activity.checkStatus(model)) {
                    tvOldTotal.setVisibility(View.GONE);
                    tvPayAmount.setText("$" + Utils.numberFormat(model.data.total));
                    rlApplyingCode.setVisibility(View.GONE);
                    tvHaveCode.setVisibility(View.VISIBLE);
                    etCouponcode.setText("");
                    orderData.total = model.data.total;
                    orderData.subTotal = model.data.subTotal;
                    orderData.couponDiscount = model.data.couponDiscount;
                    orderData.couponCode = "";
                    ((OrderDetailsActivity) activity).setOrderData(orderData);
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<CouponCode> call, Throwable t) {
                activity.failureError("remove code failed");
            }
        });
    }

    @Override
    public void bindView(View view, final int position) {
        ButterKnife.bind(this, view);
        if (imgMenu != null) {
            imgMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showOptionDialog(position);
                }
            });
        }
    }
}
