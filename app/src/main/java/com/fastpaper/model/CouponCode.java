package com.fastpaper.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CouponCode extends GeneralModel {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data {
        @Expose
        @SerializedName("user_id")
        public String userId;
        @Expose
        @SerializedName("total")
        public String total;
        @Expose
        @SerializedName("coupon_discount")
        public String couponDiscount;
        @Expose
        @SerializedName("sub_total")
        public String subTotal;
        @Expose
        @SerializedName("coupon_id")
        public String couponId;
        @Expose
        @SerializedName("order_id")
        public String orderId;
    }
}
