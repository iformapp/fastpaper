package com.fastpaper.model;

public class FilesModel {

    public String filename, date, owner;

    public FilesModel(String filename, String date, String owner) {
        this.filename = filename;
        this.date = date;
        this.owner = owner;
    }
}
