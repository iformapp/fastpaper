package com.fastpaper.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class OrderByIdModel extends GeneralModel implements Serializable {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data implements Serializable {
        @Expose
        @SerializedName("admin_files")
        public List<String> adminFiles;
        @Expose
        @SerializedName("writer_files")
        public List<WriterFiles> writerFiles;
        @Expose
        @SerializedName("user_files")
        public List<UserFiles> userFiles;
        @Expose
        @SerializedName("total")
        public String total;
        @Expose
        @SerializedName("redeem")
        public String redeem;
        @Expose
        @SerializedName("coupon_discount")
        public String couponDiscount;
        @Expose
        @SerializedName("sub_total")
        public String subTotal;
        @Expose
        @SerializedName("status")
        public String status;
        @Expose
        @SerializedName("additional_detail")
        public String additionalDetail;
        @Expose
        @SerializedName("other_subject_name")
        public String otherSubjectName;
        @Expose
        @SerializedName("subject_name")
        public String subjectName;
        @Expose
        @SerializedName("order_status_name")
        public String orderStatusName;
        @Expose
        @SerializedName("other_paper_name")
        public String otherPaperName;
        @Expose
        @SerializedName("preferred_writer")
        public String preferredWriter;
        @Expose
        @SerializedName("preferred_writer_name")
        public String preferredWriterName;
        @Expose
        @SerializedName("abstruct_page")
        public String abstructPage;
        @Expose
        @SerializedName("plagiarism_report")
        public String plagiarismReport;
        @Expose
        @SerializedName("discipline")
        public String discipline;
        @Expose
        @SerializedName("is_send_to_my_email")
        public String isSendToMyEmail;
        @Expose
        @SerializedName("powerpoint")
        public String powerpoint;
        @Expose
        @SerializedName("charts")
        public String charts;
        @Expose
        @SerializedName("source")
        public String source;
        @Expose
        @SerializedName("spacing")
        public String spacing;
        @Expose
        @SerializedName("style_name")
        public String styleName;
        @Expose
        @SerializedName("academic")
        public String academic;
        @Expose
        @SerializedName("pages")
        public String pages;
        @Expose
        @SerializedName("deadline")
        public String deadline;
        @Expose
        @SerializedName("paper_name")
        public String paperName;
        @Expose
        @SerializedName("service")
        public String service;
        @Expose
        @SerializedName("topic")
        public String topic;
        @Expose
        @SerializedName("order_deadline")
        public String orderDeadline;
        @Expose
        @SerializedName("order_date")
        public String orderDate;
        @Expose
        @SerializedName("seconds")
        public int seconds;
        @Expose
        @SerializedName("minutes")
        public int minutes;
        @Expose
        @SerializedName("hours")
        public int hours;
        @Expose
        @SerializedName("days")
        public int days;
        @Expose
        @SerializedName("coupon_code")
        public String couponCode;
        @Expose
        @SerializedName("order_number")
        public String orderNumber;
        @Expose
        @SerializedName("order_id")
        public String orderId;
    }

    public static class WriterFiles implements Serializable {
        @Expose
        @SerializedName("file_upload_simple_date")
        public String fileUploadSimpleDate;
        @Expose
        @SerializedName("file_upload_date")
        public String fileUploadDate;
        @Expose
        @SerializedName("category")
        public String category;
        @Expose
        @SerializedName("filepath")
        public String filepath;
        @Expose
        @SerializedName("filename")
        public String filename;
        @Expose
        @SerializedName("user_id")
        public String userId;
        @Expose
        @SerializedName("order_id")
        public String orderId;
        @Expose
        @SerializedName("file_id")
        public String fileId;
    }
}
