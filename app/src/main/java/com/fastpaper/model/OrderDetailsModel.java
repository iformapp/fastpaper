package com.fastpaper.model;

public class OrderDetailsModel {

    public String title, description, price;
    public boolean isSelect;

    public OrderDetailsModel(String title, String description, String price, boolean isSelect) {
        this.title = title;
        this.description = description;
        this.price = price;
        this.isSelect = isSelect;
    }
}
