package com.fastpaper.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class OrderModel extends GeneralModel implements Serializable {

    @Expose
    @SerializedName("data")
    public List<Data> data;

    public static class Data implements Serializable {
        @Expose
        @SerializedName("seconds")
        public int seconds;
        @Expose
        @SerializedName("minutes")
        public int minutes;
        @Expose
        @SerializedName("hours")
        public int hours;
        @Expose
        @SerializedName("days")
        public int days;
        @Expose
        @SerializedName("total")
        public String total;
        @Expose
        @SerializedName("coupon_discount")
        public String couponDiscount;
        @Expose
        @SerializedName("redeem")
        public String redeem;
        @Expose
        @SerializedName("sales_id")
        public String salesId;
        @Expose
        @SerializedName("order_status_name")
        public String orderStatusName;
        @Expose
        @SerializedName("payment_status_color")
        public String paymentStatusColor;
        @Expose
        @SerializedName("payment_status")
        public String paymentStatus;
        @Expose
        @SerializedName("subject_id")
        public String subjectId;
        @Expose
        @SerializedName("paper_name")
        public String paperName;
        @Expose
        @SerializedName("pages")
        public String pages;
        @Expose
        @SerializedName("sub_total")
        public String subTotal;
        @Expose
        @SerializedName("order_date")
        public String orderDate;
        @Expose
        @SerializedName("dead")
        public String dead;
        @Expose
        @SerializedName("order_deadline")
        public String orderDeadline;
        @Expose
        @SerializedName("deadline")
        public String deadline;
        @Expose
        @SerializedName("topic")
        public String topic;
        @Expose
        @SerializedName("order_number")
        public String orderNumber;
        @Expose
        @SerializedName("user_id")
        public String userId;
        @Expose
        @SerializedName("order_id")
        public String orderId;
    }
}
