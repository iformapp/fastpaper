package com.fastpaper.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PriceCalculate extends GeneralModel {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data {
        @Expose
        @SerializedName("coupon_code")
        public String couponCode;
        @Expose
        @SerializedName("coupon_discount")
        public String couponDiscount;
        @Expose
        @SerializedName("couponCodeMsg")
        public String couponcodemsg;
        @Expose
        @SerializedName("couponCodeOk")
        public boolean couponcodeok;
        @Expose
        @SerializedName("total")
        public String total;
        @Expose
        @SerializedName("subTotal")
        public String subtotal;
        @Expose
        @SerializedName("slideTotal")
        public String slidetotal;
        @Expose
        @SerializedName("slideCost")
        public String slidecost;
        @Expose
        @SerializedName("chartTotal")
        public String charttotal;
        @Expose
        @SerializedName("chartCost")
        public String chartcost;
        @Expose
        @SerializedName("pageTotal")
        public String pagetotal;
        @Expose
        @SerializedName("pageCost")
        public String pagecost;
        @Expose
        @SerializedName("couponAmt")
        public int couponamt;
        @Expose
        @SerializedName("order_id")
        public String orderId;
        @Expose
        @SerializedName("must_see_in_android")
        public String mustSeeInAndroid;
    }
}
