package com.fastpaper.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SaveOrder extends GeneralModel {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data {
        @Expose
        @SerializedName("subTotal")
        public double subtotal;
        @Expose
        @SerializedName("lifetimeCost")
        public int lifetimecost;
        @Expose
        @SerializedName("lifetimeMode")
        public String lifetimemode;
        @Expose
        @SerializedName("lifetimeCode")
        public String lifetimecode;
        @Expose
        @SerializedName("logicId")
        public String logicid;
        @Expose
        @SerializedName("orderId")
        public String orderid;
        @Expose
        @SerializedName("total")
        public double total;
        @Expose
        @SerializedName("slideTotal")
        public double slidetotal;
        @Expose
        @SerializedName("slideCost")
        public double slidecost;
        @Expose
        @SerializedName("chartTotal")
        public double charttotal;
        @Expose
        @SerializedName("chartCost")
        public double chartcost;
        @Expose
        @SerializedName("pageTotal")
        public double pagetotal;
        @Expose
        @SerializedName("pageCost")
        public double pagecost;
        @Expose
        @SerializedName("extra")
        public List<String> extra;
        @Expose
        @SerializedName("writerId")
        public String writerid;
        @Expose
        @SerializedName("writer")
        public String writer;
        @Expose
        @SerializedName("disciplineId")
        public String disciplineid;
        @Expose
        @SerializedName("slide")
        public String slide;
        @Expose
        @SerializedName("source")
        public String source;
        @Expose
        @SerializedName("chart")
        public String chart;
        @Expose
        @SerializedName("formatStyleOther")
        public String formatstyleother;
        @Expose
        @SerializedName("formatStyleId")
        public String formatstyleid;
        @Expose
        @SerializedName("word")
        public int word;
        @Expose
        @SerializedName("spacing")
        public String spacing;
        @Expose
        @SerializedName("page")
        public String page;
        @Expose
        @SerializedName("deadlineLabel")
        public String deadlinelabel;
        @Expose
        @SerializedName("deadlineType")
        public String deadlinetype;
        @Expose
        @SerializedName("deadlineValue")
        public String deadlinevalue;
        @Expose
        @SerializedName("material")
        public List<Material> material;
        @Expose
        @SerializedName("orderDetail")
        public String orderdetail;
        @Expose
        @SerializedName("topic")
        public String topic;
        @Expose
        @SerializedName("subjectOther")
        public String subjectother;
        @Expose
        @SerializedName("subjectId")
        public String subjectid;
        @Expose
        @SerializedName("paperTypeOther")
        public String papertypeother;
        @Expose
        @SerializedName("paperTypeId")
        public String papertypeid;
        @Expose
        @SerializedName("writerLevelId")
        public String writerlevelid;
        @Expose
        @SerializedName("serviceTypeId")
        public String servicetypeid;
    }

    public static class Material {
        @Expose
        @SerializedName("order_id")
        public String orderId;
        @Expose
        @SerializedName("uploaded_by")
        public String uploadedBy;
        @Expose
        @SerializedName("date_time")
        public String dateTime;
        @Expose
        @SerializedName("user_id")
        public String userId;
        @Expose
        @SerializedName("category")
        public String category;
        @Expose
        @SerializedName("file_path")
        public String filePath;
        @Expose
        @SerializedName("file_name")
        public String fileName;
    }
}
