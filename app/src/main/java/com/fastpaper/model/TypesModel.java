package com.fastpaper.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TypesModel extends GeneralModel implements Serializable {

    @Expose
    @SerializedName("data")
    public List<Data> data;

    public static class Data {

        @Expose
        @SerializedName("style_price")
        public String stylePrice;
        @Expose
        @SerializedName("style_name")
        public String styleName;
        @Expose
        @SerializedName("style_id")
        public String styleId;
        @Expose
        @SerializedName("discipline_price")
        public String disciplinePrice;
        @Expose
        @SerializedName("discipline")
        public String discipline;
        @Expose
        @SerializedName("discipline_id")
        public String disciplineId;
        @Expose
        @SerializedName("academic_level_id")
        public String academicLevelId;
        @Expose
        @SerializedName("academic_level_name")
        public String academicLevelName;
        @Expose
        @SerializedName("category")
        public String category;
        @Expose
        @SerializedName("id")
        public String id;
        @Expose
        @SerializedName("subject_name")
        public String subjectName;
        @Expose
        @SerializedName("acct_id")
        public String acctId;
        @Expose
        @SerializedName("sort_order")
        public String sortOrder;
        @Expose
        @SerializedName("paper_price")
        public String paperPrice;
        @Expose
        @SerializedName("paper_name")
        public String paperName;
        @Expose
        @SerializedName("paper_id")
        public String paperId;

        public boolean isSelected;
    }


}
