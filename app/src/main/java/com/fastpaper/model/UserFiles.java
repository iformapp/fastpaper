package com.fastpaper.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserFiles implements Serializable {

    @Expose
    @SerializedName("file_upload_simple_date")
    public String fileUploadSimpleDate;
    @Expose
    @SerializedName("file_upload_date")
    public String fileUploadDate;
    @Expose
    @SerializedName("category")
    public String category;
    @Expose
    @SerializedName("filepath")
    public String filepath;
    @Expose
    @SerializedName("filename")
    public String filename;
    @Expose
    @SerializedName("user_id")
    public String userId;
    @Expose
    @SerializedName("order_id")
    public String orderId;
    @Expose
    @SerializedName("file_id")
    public String fileId;
}
