package com.fastpaper.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserModel extends GeneralModel implements Serializable {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data implements Serializable {
        @Expose
        @SerializedName("user_id")
        public String userId;
        @Expose
        @SerializedName("accessToken")
        public String accesstoken;
        @Expose
        @SerializedName("first_name")
        public String firstName;
        @Expose
        @SerializedName("last_name")
        public String lastName;
        @Expose
        @SerializedName("countryname")
        public String countryname;
        @Expose
        @SerializedName("timezone")
        public String timezone;
        @Expose
        @SerializedName("mobile")
        public String mobile;
        @Expose
        @SerializedName("dialCode")
        public String dialcode;
        @Expose
        @SerializedName("password")
        public String password;
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("email")
        public String email;
        @Expose
        @SerializedName("method")
        public String method;
    }
}
