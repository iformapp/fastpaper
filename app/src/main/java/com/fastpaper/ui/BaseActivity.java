package com.fastpaper.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.fastpaper.R;
import com.fastpaper.api.ApiClient;
import com.fastpaper.api.ApiInterface;
import com.fastpaper.model.GeneralModel;
import com.fastpaper.model.UserModel;
import com.fastpaper.util.Constants;
import com.fastpaper.util.Preferences;
import com.tooltip.Tooltip;
import com.victor.loading.rotate.RotateLoading;

import java.util.TimeZone;

public class BaseActivity extends AppCompatActivity {

    private Dialog dialog;
    public static final int REQ_LOGIN = 888;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public ApiInterface getService() {
        return ApiClient.getClient().create(ApiInterface.class);
    }

    public void failureError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        hideProgress();
    }

    public void validationError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public String getAccessToken() {
        UserModel.Data userData = Preferences.getUserData(this);
        return userData != null ? userData.accesstoken : "";
    }

    public String getUserId() {
        UserModel.Data userData = Preferences.getUserData(this);
        return userData != null ? userData.userId : "";
    }

    public String getOrderId() {
        return Preferences.readString(this, Constants.ORDER_ID, "");
    }

    public boolean isLogin() {
        return Preferences.readBoolean(this, Constants.IS_LOGIN, false);
    }

    public boolean checkStatus(GeneralModel model) {
        if (model == null)
            return false;

        if (model.success != null) {
            switch (model.success) {
                case "0":
                    if (model.msg.equalsIgnoreCase(getString(R.string.invalid_access_token))) {
                        //Preferences.clearPreferences(this);
                        Preferences.saveUserData(this, null);
                        goToLoginSignup(true);
                    }
                    return false;
                case "1":
                    return true;
            }
        } else if (model.flag != null) {
            if (model.flag.equals("1")) {
                return true;
            }
        }
        if (model.msg != null && !isEmpty(model.msg))
            failureError(model.msg);
        return false;
    }

    public String getToken() {
        return Preferences.readString(this, Constants.FCM_TOKEN, "");
    }

    public String getDeviceType() {
        return "android"; // for Android
    }

    public String getTimeZone() {
        return TimeZone.getDefault().getID(); // for Android
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null && cm.getActiveNetworkInfo() != null) {
            return true;
        }
        Toast.makeText(this, "connect to internet", Toast.LENGTH_SHORT).show();
        return false;
    }

    public void gotoMainActivity(int screen) {
        if (getParent() != null) {
            ((MainActivity) getParent()).gotoMainActivity(screen);
        } else {
            Intent i = new Intent(this, MainActivity.class);
            i.putExtra(Constants.SCREEN_NAME, screen);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
            finishToRight();
        }
    }

    public void goToLoginSignup(boolean isLogin) {
        Intent i = new Intent(this, LoginSignUpActivity.class);
        i.putExtra(Constants.FROM_LOGIN, isLogin);
        startActivity(i);
        openToTop();
    }

    public void redirectTab(int tabIndex) {
        if (getParent() != null) {
            ((MainActivity) getParent()).getTabHost().setCurrentTab(tabIndex);
        }
    }

    public void redirectActivity(Class<?> activityClass) {
        if (getParent() != null) {
            ((MainActivity) getParent()).redirectActivity(activityClass);
        } else {
            startActivity(new Intent(this, activityClass));
            openToLeft();
        }
    }

    public void openToTop() {
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
    }

    public void openToLeft() {
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
    }

    public void finishToBottom() {
        overridePendingTransition(R.anim.stay, R.anim.slide_out_down);
    }

    public void finishToRight() {
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    public void whiteBackgroundView(View... views) {
        for (View v : views) {
            v.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        }
    }

    public void blackTextView(View... views) {
        for (View v : views) {
            ((TextView) v).setTextColor(ContextCompat.getColor(this, R.color.black));
        }
    }

    public void showProgress() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_loading);
        dialog.setCancelable(false);
        RotateLoading rotateLoading = dialog.findViewById(R.id.rotateloading);
        rotateLoading.start();
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public void hideProgress() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public boolean isEmpty(String s) {
        return TextUtils.isEmpty(s);
    }

    public void redirectUsingCustomTab(String url) {
        try {
            Uri uri = Uri.parse(url);
            CustomTabsIntent.Builder intentBuilder = new CustomTabsIntent.Builder();
            intentBuilder.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
            intentBuilder.setSecondaryToolbarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            CustomTabsIntent customTabsIntent = intentBuilder.build();
            customTabsIntent.launchUrl(this, uri);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "url not valid", Toast.LENGTH_SHORT).show();
        }
    }

    public void showToolTip(View anchor, String text) {
        Typeface tf = Typeface.createFromAsset(getAssets(), Constants.SFTEXT_REGULAR);

        new Tooltip.Builder(anchor)
                .setText(text)
                .setBackgroundColor(ContextCompat.getColor(this, R.color.toast_color))
                .setCancelable(true)
                .setDismissOnClick(true)
                .setTextColor(Color.WHITE)
                .setTypeface(tf)
                .setCornerRadius(5.0f)
                .show();
    }
}
