package com.fastpaper.ui;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;

import com.fastpaper.R;
import com.fastpaper.ui.chat.ChatActivity;
import com.fastpaper.ui.home.HomeActivity;
import com.fastpaper.ui.neworder.NewOrderActivity;
import com.fastpaper.ui.order.OrderActivity;
import com.fastpaper.ui.profile.ProfileActivity;
import com.fastpaper.util.Constants;
import com.fastpaper.util.Preferences;

public class MainActivity extends TabActivity implements TabHost.OnTabChangeListener {

    private TabHost mTabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTabHost = (TabHost) findViewById(android.R.id.tabhost);

        setTab("home", R.drawable.home_tab, HomeActivity.class);
        setTab("chat", R.drawable.chat_tab, ChatActivity.class);
        setTab("plus", R.drawable.chat_card_bg, NewOrderActivity.class);
        setTab("order", R.drawable.order_tab, OrderActivity.class);
        setTab("profile", R.drawable.profile_tab, ProfileActivity.class);

        mTabHost.setOnTabChangedListener(this);

        if (getIntent() != null) {
            int screen = getIntent().getIntExtra(Constants.SCREEN_NAME, 0);
            mTabHost.setCurrentTab(screen);
        }

        ImageView imgPlus = findViewById(R.id.img_plus);
        imgPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.writeString(MainActivity.this, Constants.ORDER_ID, "");
                Intent i = new Intent(MainActivity.this, NewOrderActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
            }
        });
    }

    public void setTab(String tag, int drawable, Class<?> activityClass) {
        TabHost.TabSpec tabSpec = mTabHost.newTabSpec(tag)
                .setIndicator("", ContextCompat.getDrawable(this, drawable))
                .setContent(new Intent(this, activityClass));
        mTabHost.addTab(tabSpec);
    }

    @Override
    public void onTabChanged(String tabId) {

    }

    public void goToLoginSignup(boolean isLogin) {
        Intent i = new Intent(this, LoginSignUpActivity.class);
        i.putExtra(Constants.FROM_LOGIN, isLogin);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
    }

    public void gotoMainActivity(int screen) {
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra(Constants.SCREEN_NAME, screen);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    public void redirectActivity(Class<?> activityClass) {
        startActivity(new Intent(this, activityClass));
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
    }
}
