package com.fastpaper.ui.chat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.fastpaper.R;
import com.fastpaper.ui.BaseActivity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;

public class ChatActivity extends BaseActivity implements View.OnClickListener {

    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.img_chat, R.id.btn_chat_here})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_chat:
                showDialog();
                break;
            case R.id.btn_chat_here:
                Intercom.client().displayMessenger();
                break;
        }
    }

    public void showDialog() {
        dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_chat_now);
        dialog.setCancelable(true);
        View call = dialog.findViewById(R.id.rl_call);
        View messanger = dialog.findViewById(R.id.rl_messanger);
        View whatsapp = dialog.findViewById(R.id.rl_whatsapp);
        View email = dialog.findViewById(R.id.rl_email);
        View sms = dialog.findViewById(R.id.rl_sms);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.btn_cancel);

        call.setOnClickListener(this);
        messanger.setOnClickListener(this);
        whatsapp.setOnClickListener(this);
        email.setOnClickListener(this);
        sms.setOnClickListener(this);
        tvCancel.setOnClickListener(this);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_call:
                Dexter.withActivity(this)
                        .withPermission(Manifest.permission.CALL_PHONE)
                        .withListener(new PermissionListener() {
                            @SuppressLint("MissingPermission")
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse response) {
                                try {
                                    Intent intent = new Intent(Intent.ACTION_CALL,
                                            Uri.parse("tel:" + getString(R.string.phone_number)));
                                    startActivity(intent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(ChatActivity.this, "Failed to invoke call", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse response) {
                                Toast.makeText(ChatActivity.this, "Please Give Permission to make call", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();
                break;
            case R.id.rl_messanger:
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("fb://messaging/CheapestEssay")));
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.messenger.com/t/CheapestEssay")));
                }
                break;
            case R.id.rl_whatsapp:
                try {
                    String toNumber = getString(R.string.phone_number);
                    toNumber = toNumber.replace("+", "")
                            .replace("(", "")
                            .replace(")", "")
                            .replace("-", "")
                            .replace(" ", "");

                    Intent sendIntent =new Intent("android.intent.action.MAIN");
                    sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.setType("text/plain");
                    sendIntent.putExtra(Intent.EXTRA_TEXT,"");
                    sendIntent.putExtra("jid", toNumber +"@s.whatsapp.net");
                    sendIntent.setPackage("com.whatsapp");
                    startActivity(sendIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Please install WhatsApp", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.whatsapp")));
                }
                break;
            case R.id.rl_email:
                try {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto", getString(R.string.email_text).toLowerCase(), null));
                    startActivity(Intent.createChooser(emailIntent, "Send email..."));
                } catch (Exception e) {
                    Toast.makeText(this, "Mail apps not installed", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.rl_sms:
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", getString(R.string.phone_number), null)));
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Inbox not found", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_cancel:
                if (dialog != null) {
                    dialog.dismiss();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
