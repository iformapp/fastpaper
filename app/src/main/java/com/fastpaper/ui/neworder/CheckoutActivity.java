package com.fastpaper.ui.neworder;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.fastpaper.R;
import com.fastpaper.fragment.CheckoutFragment;
import com.fastpaper.model.OrderByIdModel;
import com.fastpaper.ui.BaseActivity;
import com.fastpaper.util.Constants;
import com.fastpaper.util.textview.TextViewSFDisplayRegular;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;

public class CheckoutActivity extends BaseActivity {

    @BindView(R.id.tv_right)
    TextViewSFDisplayRegular tvRight;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    private static final String CONFIG_CLIENT_ID = "ATQXJWy7AISBwVu5mtA7LU_uu1Q-AoEzS3QjwLprX_AzpHb8jp-Otjy2rBxwKHor0Id2YfX8ewGm77im";
    public static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID);

    private ViewPagerAdapter adapter;
    private OrderByIdModel.Data orderData;
    private int[] tabIcons = {
            R.drawable.visa,
            R.drawable.paypal
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);

        tvRight.setText(getString(R.string.chat));

        if (getIntent() != null) {
            orderData = (OrderByIdModel.Data) getIntent().getSerializableExtra(Constants.ORDER_DATA);
        }

        if (orderData != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(CheckoutActivity.this, PayPalService.class);
                    intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
                    startService(intent);

                    setupViewPager(viewPager);

                    tabLayout.setupWithViewPager(viewPager);
                    setupTabIcons();
                }
            }, 200);
        }
    }

    public OrderByIdModel.Data getOrderData() {
        return orderData;
    }

    private void setupTabIcons() {
        int tabCount = adapter.getCount();

        for (int i = 0; i < tabCount; i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                ImageView myCustomIcon = (ImageView) LayoutInflater.from(tabLayout.getContext()).inflate(R.layout.custom_tab_icon, null);
                myCustomIcon.setImageDrawable(ContextCompat.getDrawable(this, tabIcons[i]));
                tab.setCustomView(myCustomIcon);
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(CheckoutFragment.newInstanace(true), "Visa");
        adapter.addFrag(CheckoutFragment.newInstanace(false), "Paypal");
        viewPager.setAdapter(adapter);
    }

    @OnClick({R.id.ll_back, R.id.tv_right})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_right:
                Intercom.client().displayMessenger();
                break;
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToBottom();
    }

    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }
}
