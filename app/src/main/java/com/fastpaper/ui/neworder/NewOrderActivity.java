package com.fastpaper.ui.neworder;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fastpaper.R;
import com.fastpaper.adapter.RecyclerviewAdapter;
import com.fastpaper.adapter.TypesAdapter;
import com.fastpaper.adapter.UploadFileAdapter;
import com.fastpaper.model.CouponCode;
import com.fastpaper.model.FileUpload;
import com.fastpaper.model.OrderByIdModel;
import com.fastpaper.model.PriceCalculate;
import com.fastpaper.model.SaveOrder;
import com.fastpaper.model.TypesModel;
import com.fastpaper.ui.BaseActivity;
import com.fastpaper.util.Constants;
import com.fastpaper.util.Preferences;
import com.fastpaper.util.Utils;
import com.fastpaper.util.edittext.EditTextSFTextRegular;
import com.fastpaper.util.textview.TextViewSFDisplayBold;
import com.fastpaper.util.textview.TextViewSFDisplayRegular;
import com.fastpaper.util.textview.TextViewSFTextBold;
import com.fastpaper.util.textview.TextViewSFTextRegular;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.ImageFile;
import com.vincent.filepicker.filter.entity.NormalFile;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.vincent.filepicker.activity.ImagePickActivity.IS_NEED_CAMERA;

public class NewOrderActivity extends BaseActivity implements RecyclerviewAdapter.OnViewBindListner {

    @BindView(R.id.btn_writing)
    TextViewSFTextRegular btnWriting;
    @BindView(R.id.btn_editing)
    TextViewSFTextRegular btnEditing;
    @BindView(R.id.btn_powerpoint)
    TextViewSFTextRegular btnPowerpoint;
    @BindView(R.id.btn_college)
    TextViewSFTextRegular btnCollege;
    @BindView(R.id.btn_bachelor)
    TextViewSFTextRegular btnBachelor;
    @BindView(R.id.btn_master)
    TextViewSFTextRegular btnMaster;
    @BindView(R.id.tv_paper_type)
    TextViewSFTextRegular tvPaperType;
    @BindView(R.id.tv_subject)
    TextViewSFTextRegular tvSubject;
    @BindView(R.id.tv_3hours)
    TextViewSFTextRegular tv3hours;
    @BindView(R.id.tv_6hours)
    TextViewSFTextRegular tv6hours;
    @BindView(R.id.tv_12hours)
    TextViewSFTextRegular tv12hours;
    @BindView(R.id.tv_24hours)
    TextViewSFTextRegular tv24hours;
    @BindView(R.id.tv_2days)
    TextViewSFTextRegular tv2days;
    @BindView(R.id.tv_4days)
    TextViewSFTextRegular tv4days;
    @BindView(R.id.tv_7days)
    TextViewSFTextRegular tv7days;
    @BindView(R.id.tv_10days)
    TextViewSFTextRegular tv10days;
    @BindView(R.id.tv_15days)
    TextViewSFTextRegular tv15days;
    @BindView(R.id.tv_words)
    TextViewSFTextRegular tvWords;
    @BindView(R.id.tv_pages)
    TextViewSFTextRegular tvPages;
    @BindView(R.id.et_topic)
    EditTextSFTextRegular etTopic;
    @BindView(R.id.et_details)
    EditTextSFTextRegular etDetails;
    @BindView(R.id.tv_format_style)
    TextViewSFDisplayRegular tvFormatStyle;
    @BindView(R.id.tv_discipline)
    TextViewSFDisplayRegular tvDiscipline;
    @BindView(R.id.tv_preferred_writer)
    TextViewSFDisplayRegular tvPreferredWriter;
    @BindView(R.id.img_abstract_page)
    ImageView imgAbstractPage;
    @BindView(R.id.img_turnitin)
    ImageView imgTurnitin;
    @BindView(R.id.img_send_email)
    ImageView imgSendEmail;
    @BindView(R.id.et_couponcode)
    EditTextSFTextRegular etCouponcode;
    @BindView(R.id.ll_coupon_code)
    LinearLayout llCouponCode;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.tv_toolbar_price)
    TextViewSFTextBold tvToolbarPrice;
    @BindView(R.id.rv_orders)
    RecyclerView rvOrders;
    @BindView(R.id.ll_more)
    LinearLayout llMore;
    @BindView(R.id.tv_have_code)
    TextViewSFTextRegular tvHaveCode;
    @BindView(R.id.tv_coupon_code)
    TextViewSFTextRegular tvCouponCode;
    @BindView(R.id.rl_applying_code)
    RelativeLayout rlApplyingCode;
    @BindView(R.id.img_arrow)
    ImageView imgArrow;
    @BindView(R.id.tv_spaced_page)
    TextViewSFTextRegular tvSpacedPage;
    @BindView(R.id.et_other_paper)
    EditTextSFTextRegular etOtherPaper;
    @BindView(R.id.et_other_subject)
    EditTextSFTextRegular etOtherSubject;
    @BindView(R.id.tv_files_count)
    TextViewSFTextRegular tvFilesCount;
    @BindView(R.id.et_other_format)
    EditTextSFTextRegular etOtherFormat;
    @BindView(R.id.et_other_writer)
    EditTextSFTextRegular etOtherWriter;
    @BindView(R.id.tv_category)
    TextViewSFDisplayRegular tvCategory;
    @BindView(R.id.rv_files)
    RecyclerView rvFiles;
    @BindView(R.id.tv_no_files)
    TextViewSFDisplayBold tvNoFiles;
    @BindView(R.id.tv_total)
    TextViewSFDisplayRegular tvTotal;
    @BindView(R.id.root)
    LinearLayout root;
    @BindView(R.id.tv_more_options)
    TextViewSFTextRegular tvMoreOptions;
    @BindView(R.id.tv_old_total)
    TextViewSFDisplayRegular tvOldTotal;

    private RecyclerviewAdapter mAdapter;
    private UploadFileAdapter fileAdapter;
    private ArrayList<String> pageTypeArray;
    private boolean isAbstractPage = false;
    private boolean isTurnitinReport = false;
    private boolean isSendEmail = false;
    private TypesAdapter typesAdapter;
    private String deadlineType, deadlineValue;
    private int writerLevelId = Constants.COLLEGE_ID;
    private int serviceTypeId = Constants.WRITING_ID;
    private String paperTypeId = "1";
    private String prefferedId = "any_writer";
    private String subjectId = "1";
    private String disciplineId = "1";
    private String formatStyleId = "10";
    private String spacing = "1"; // 1 for double and 2 for single space
    private String chartCount = "0";
    private String sourceCount = "0";
    private String slideCount = "0";
    private Map<String, String> map;
    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order);
        ButterKnife.bind(this);

        init();
    }

    public void init() {
        initDeadlineViews();

        List<TypesModel.Data> paperData = Preferences.getTypes(this, Constants.PAPER_TYPES);
        if (paperData != null && paperData.size() > 0) {
            for (int i = 0; i < paperData.size(); i++) {
                if (paperData.get(i).paperName.equalsIgnoreCase(tvPaperType.getText().toString())) {
                    paperTypeId = paperData.get(i).paperId;
                    break;
                }
            }
        }

        List<TypesModel.Data> disciplinData = Preferences.getTypes(this, Constants.DISCIPLIN_TYPES);
        if (disciplinData != null && disciplinData.size() > 0) {
            for (int i = 0; i < disciplinData.size(); i++) {
                if (disciplinData.get(i).discipline.equalsIgnoreCase(tvDiscipline.getText().toString())) {
                    disciplineId = disciplinData.get(i).disciplineId;
                    break;
                }
            }
        }

        List<TypesModel.Data> formatData = Preferences.getTypes(this, Constants.FORMATED_STYLE_TYPES);
        if (formatData != null && formatData.size() > 0) {
            for (int i = 0; i < formatData.size(); i++) {
                if (formatData.get(i).styleName.equalsIgnoreCase(tvFormatStyle.getText().toString())) {
                    formatStyleId = formatData.get(i).styleId;
                    break;
                }
            }
        }

        List<TypesModel.Data> subjectData = Preferences.getTypes(this, Constants.SUBJECTS_TYPES);
        if (subjectData != null && subjectData.size() > 0) {
            for (int i = 0; i < subjectData.size(); i++) {
                if (subjectData.get(i).subjectName.equalsIgnoreCase(tvSubject.getText().toString())) {
                    subjectId = subjectData.get(i).id;
                    break;
                }
            }
        }

        if (getIntent() != null) {
            map = (Map<String, String>) getIntent().getSerializableExtra(Constants.ORDER_DATA);
            if (map != null) {
                writerLevelId = Integer.parseInt(map.get(Constants.WRITER_LEVEL_ID));
                deadlineType = map.get(Constants.DEADLINE_TYPE);
                deadlineValue = map.get(Constants.DEADLINE_VALUE);
                tvPages.setText(map.get(Constants.PAGE));
                tvPaperType.setText(map.get(Constants.PAPER_TYPES));
                if (paperData != null && paperData.size() > 0) {
                    for (int i = 0; i < paperData.size(); i++) {
                        if (paperData.get(i).paperName.equalsIgnoreCase(tvPaperType.getText().toString())) {
                            paperTypeId = paperData.get(i).paperId;
                            break;
                        }
                    }
                }
                int resID = getResources().getIdentifier("tv_" + deadlineValue.toLowerCase() + deadlineType.toLowerCase(),
                        "id", getPackageName());
                TextView textView = findViewById(resID);
                selectedView(textView, deadlineValue, deadlineType);
                switch (writerLevelId) {
                    case Constants.COLLEGE_ID:
                        btnCollege.performClick();
                        break;
                    case Constants.MASTER_ID:
                        btnMaster.performClick();
                        break;
                    case Constants.BECHELOR_ID:
                        btnBachelor.performClick();
                        break;
                }
            } else {
                tv15days.performClick();
            }
        } else {
            tv15days.performClick();
        }

        tvOldTotal.setPaintFlags(tvOldTotal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        rvOrders.setLayoutManager(new LinearLayoutManager(this));
        rvFiles.setLayoutManager(new LinearLayoutManager(this));

        pageTypeArray = new ArrayList<>();
        pageTypeArray.add(getString(R.string.sorces));
        pageTypeArray.add(getString(R.string.charts));
        pageTypeArray.add(getString(R.string.powerpoint_slide));
        mAdapter = new RecyclerviewAdapter(pageTypeArray, R.layout.item_new_order_types, this);
        rvOrders.setAdapter(mAdapter);

        tvCouponCode.setPaintFlags(tvCouponCode.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        etOtherSubject.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // nothing to do here
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        getPrice();
                    }
                }, 600);
            }
        });

        etTopic.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // nothing to do here
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        getPrice();
                    }
                }, 600);
            }
        });

        etDetails.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // nothing to do here
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        getPrice();
                    }
                }, 600);
            }
        });
    }

    public String getPage() {
        return tvPages.getText().toString();
    }

    public void getPrice() {
        if (!isNetworkConnected())
            return;

        setPageType();

        Map<String, String> map = new HashMap<>();
        map.put("serviceTypeId", String.valueOf(serviceTypeId));
        map.put("paperTypeId", paperTypeId);
        map.put("paperTypeOther", etOtherPaper.getText().toString());
        map.put("subjectId", subjectId);
        map.put("subjectOther", etOtherSubject.getText().toString());
        map.put("chart", chartCount);
        map.put("source", sourceCount);
        map.put("slide", slideCount);
        map.put("formatStyleId", formatStyleId);
        map.put("formatStyleOther", etOtherFormat.getText().toString());
        map.put("disciplineId", disciplineId);
        map.put("writer", prefferedId);
        map.put("topic", etTopic.getText().toString());
        map.put("spacing", spacing);
        map.put("orderDetail", etDetails.getText().toString());
        map.put("deadlineType", deadlineType);
        map.put("deadlineValue", deadlineValue);
        map.put("writerLevelId", String.valueOf(writerLevelId));
        map.put("page", getPage());
        map.put("extraSendItToEmail", isSendEmail ? "10" : "0");
        map.put("extraAbstract", isAbstractPage ? "9" : "0");
        map.put("extraTurnintin", isTurnitinReport ? "8" : "0");
        map.put("writer_id", etOtherWriter.getText().toString());
        map.put("user_id", getUserId());
        map.put("order_id", getOrderId());

        Log.e("Get Price Url = > ", Constants.BASE_URL + Constants.PRICE_CALCULATE);
        Log.e("Price Params", map.toString());

        Call<Object> call = getService().getPrice(map);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                PriceCalculate getPrice = new Gson().fromJson(new Gson().toJson(response.body()), PriceCalculate.class);
                String json = new Gson().toJson(response.body());
                Log.e("Price response", json);
                if (checkStatus(getPrice)) {
                    Preferences.writeString(NewOrderActivity.this, Constants.ORDER_ID, getPrice.data.orderId);
                    tvToolbarPrice.setText("$" + Utils.numberFormat(getPrice.data.total));
                    tvTotal.setText("$" + Utils.numberFormat(getPrice.data.total));
                    tvOldTotal.setText("$" + Utils.numberFormat(getPrice.data.subtotal));
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("get price failed");
            }
        });
    }

    @Override
    public void bindView(View view, final int position) {
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        TextView tvPrice = (TextView) view.findViewById(R.id.tv_price);
        final TextView tvPages = (TextView) view.findViewById(R.id.tv_pages);
        ImageView imgMinus = (ImageView) view.findViewById(R.id.img_minus);
        ImageView imgPlus = (ImageView) view.findViewById(R.id.img_plus);

        tvTitle.setText(pageTypeArray.get(position));
        if (position == 0) {
            tvPrice.setText("( FREE )");
        } else {
            tvPrice.setText("");
        }

        tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    showToolTip(v, getString(R.string.tooltip_sources));
                } else if (position == 1) {
                    showToolTip(v, getString(R.string.tooltip_charts));
                } else {
                    showToolTip(v, getString(R.string.tooltip_powerpoint));
                }
            }
        });

        imgMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int page = Integer.parseInt(tvPages.getText().toString());
                if (page != 0) {
                    tvPages.setText(String.valueOf(page - 1));
                    getPrice();
                }
            }
        });

        imgPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int page = Integer.parseInt(tvPages.getText().toString());
                tvPages.setText(String.valueOf(page + 1));
                getPrice();
            }
        });
    }

    public void setPageType() {
        for (int i = 0; i < rvOrders.getChildCount(); i++) {
            View v = rvOrders.getChildAt(i);
            TextView tvPages = (TextView) v.findViewById(R.id.tv_pages);
            if (i == 0) {
                sourceCount = tvPages.getText().toString();
            } else if (i == 1) {
                chartCount = tvPages.getText().toString();
            } else if (i == 2) {
                slideCount = tvPages.getText().toString();
            }
        }
    }

    @OnClick({R.id.tv_back, R.id.tv_question, R.id.tv_type_service_info, R.id.btn_writing, R.id.btn_editing, R.id.btn_powerpoint,
            R.id.tv_writer_info, R.id.btn_college, R.id.btn_bachelor, R.id.btn_master, R.id.tv_type_paper_info, R.id.tv_subject_info,
            R.id.tv_3hours, R.id.tv_6hours, R.id.tv_12hours, R.id.tv_24hours, R.id.tv_2days, R.id.tv_4days, R.id.tv_7days, R.id.tv_10days,
            R.id.tv_15days, R.id.tv_pages_info, R.id.img_minus, R.id.img_plus, R.id.tv_topic_info, R.id.tv_details_info,
            R.id.tv_upload_info, R.id.rl_upload_files, R.id.rl_abstract_page, R.id.rl_turnitin, R.id.rl_email, R.id.ll_more,
            R.id.tv_apply, R.id.img_coupon_close, R.id.rl_save_order, R.id.tv_have_code, R.id.ll_show_option, R.id.tv_coupon_remove,
            R.id.tv_paper_type, R.id.tv_subject, R.id.tv_format_style, R.id.tv_discipline, R.id.tv_preferred_writer, R.id.rl_page_type,
            R.id.tv_category, R.id.tv_format_info, R.id.tv_discipline_info, R.id.tv_preferred_writer_info, R.id.tv_abstract_info,
            R.id.tv_turnitin_info, R.id.tv_email_info, R.id.tv_deadline_info})
    public void onViewClicked(View view) {
        Utils.hideSoftKeyboard(this);
        switch (view.getId()) {
            case R.id.tv_back:
                onBackPressed();
                break;
            case R.id.tv_question:
                Intercom.client().displayMessageComposer();
                break;
            case R.id.tv_type_service_info:
                showToolTip(view, getString(R.string.tooltip_service));
                break;
            case R.id.btn_writing:
                serviceTypeId = Constants.WRITING_ID;
                unselectServiceTypeAll();
                btnWriting.setBackground(ContextCompat.getDrawable(this, R.drawable.left_corner_select));
                btnWriting.setTextColor(ContextCompat.getColor(this, R.color.white));
                getPrice();
                break;
            case R.id.btn_editing:
                serviceTypeId = Constants.EDITING_ID;
                unselectServiceTypeAll();
                btnEditing.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                btnEditing.setTextColor(ContextCompat.getColor(this, R.color.white));
                getPrice();
                break;
            case R.id.btn_powerpoint:
                serviceTypeId = Constants.POWERPOINT_ID;
                unselectServiceTypeAll();
                btnPowerpoint.setBackground(ContextCompat.getDrawable(this, R.drawable.right_corner_select));
                btnPowerpoint.setTextColor(ContextCompat.getColor(this, R.color.white));
                getPrice();
                break;
            case R.id.tv_writer_info:
                showToolTip(view, getString(R.string.tooltip_writer));
                break;
            case R.id.btn_college:
                writerLevelId = Constants.COLLEGE_ID;
                unselectWriterAll();
                btnCollege.setBackground(ContextCompat.getDrawable(this, R.drawable.left_corner_select));
                btnCollege.setTextColor(ContextCompat.getColor(this, R.color.white));
                getPrice();
                break;
            case R.id.btn_bachelor:
                writerLevelId = Constants.BECHELOR_ID;
                unselectWriterAll();
                btnBachelor.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                btnBachelor.setTextColor(ContextCompat.getColor(this, R.color.white));
                getPrice();
                break;
            case R.id.btn_master:
                writerLevelId = Constants.MASTER_ID;
                unselectWriterAll();
                btnMaster.setBackground(ContextCompat.getDrawable(this, R.drawable.right_corner_select));
                btnMaster.setTextColor(ContextCompat.getColor(this, R.color.white));
                getPrice();
                break;
            case R.id.tv_type_paper_info:
                showToolTip(view, getString(R.string.tooltip_paper));
                break;
            case R.id.tv_subject_info:
                showToolTip(view, getString(R.string.tooltip_subject));
                break;
            case R.id.tv_3hours:
                deadlineType = getString(R.string.hours);
                deadlineValue = "3";
                unselectDeadlineAll();
                selectedView(tv3hours, deadlineValue, deadlineType);
                break;
            case R.id.tv_6hours:
                deadlineType = getString(R.string.hours);
                deadlineValue = "6";
                unselectDeadlineAll();
                selectedView(tv6hours, deadlineValue, deadlineType);
                break;
            case R.id.tv_12hours:
                deadlineType = getString(R.string.hours);
                deadlineValue = "12";
                unselectDeadlineAll();
                selectedView(tv12hours, deadlineValue, deadlineType);
                break;
            case R.id.tv_24hours:
                deadlineType = getString(R.string.hours);
                deadlineValue = "24";
                unselectDeadlineAll();
                selectedView(tv24hours, deadlineValue, deadlineType);
                break;
            case R.id.tv_2days:
                deadlineType = getString(R.string.days);
                deadlineValue = "2";
                unselectDeadlineAll();
                selectedView(tv2days, deadlineValue, deadlineType);
                break;
            case R.id.tv_4days:
                deadlineType = getString(R.string.days);
                deadlineValue = "4";
                unselectDeadlineAll();
                selectedView(tv4days, deadlineValue, deadlineType);
                break;
            case R.id.tv_10days:
                deadlineType = getString(R.string.days);
                deadlineValue = "10";
                unselectDeadlineAll();
                selectedView(tv10days, deadlineValue, deadlineType);
                break;
            case R.id.tv_7days:
                deadlineType = getString(R.string.days);
                deadlineValue = "7";
                unselectDeadlineAll();
                selectedView(tv7days, deadlineValue, deadlineType);
                break;
            case R.id.tv_15days:
                deadlineType = getString(R.string.days);
                deadlineValue = "15";
                unselectDeadlineAll();
                selectedView(tv15days, deadlineValue, deadlineType);
                break;
            case R.id.tv_pages_info:
                showToolTip(view, getString(R.string.tooltip_pages));
                break;
            case R.id.img_minus:
                int page = Integer.parseInt(tvPages.getText().toString());
                if (page != 0) {
                    tvPages.setText(String.valueOf(page - 1));
                    getPrice();
                }
                break;
            case R.id.img_plus:
                page = Integer.parseInt(tvPages.getText().toString());
                tvPages.setText(String.valueOf(page + 1));
                getPrice();
                break;
            case R.id.rl_page_type:
                Dialog dialog = onCreateDialogSingleChoice();
                dialog.show();
                break;
            case R.id.tv_topic_info:
                showToolTip(view, getString(R.string.tooltip_topic));
                break;
            case R.id.tv_details_info:
                showToolTip(view, getString(R.string.tooltip_details));
                break;
            case R.id.tv_upload_info:
                showToolTip(view, getString(R.string.tooltip_upload));
                break;
            case R.id.tv_format_info:
                showToolTip(view, getString(R.string.tooltip_format));
                break;
            case R.id.tv_discipline_info:
                showToolTip(view, getString(R.string.tooltip_discipline));
                break;
            case R.id.tv_preferred_writer_info:
                showToolTip(view, getString(R.string.tooltip_preferred_writer));
                break;
            case R.id.tv_abstract_info:
                showToolTip(view, getString(R.string.tooltip_abstract));
                break;
            case R.id.tv_turnitin_info:
                showToolTip(view, getString(R.string.tooltip_turnitin));
                break;
            case R.id.tv_email_info:
                showToolTip(view, getString(R.string.tooltip_send_it_mail));
                break;
            case R.id.tv_deadline_info:
                showToolTip(view, getString(R.string.tooltip_deadline));
                break;
            case R.id.rl_upload_files:
                selectFileDialog();
                break;
            case R.id.rl_abstract_page:
                imgAbstractPage.setImageDrawable(ContextCompat.getDrawable(this,
                        isAbstractPage ? R.drawable.radio_unselect : R.drawable.radio_selected));
                isAbstractPage = !isAbstractPage;
                getPrice();
                break;
            case R.id.rl_turnitin:
                imgTurnitin.setImageDrawable(ContextCompat.getDrawable(this,
                        isTurnitinReport ? R.drawable.radio_unselect : R.drawable.radio_selected));
                isTurnitinReport = !isTurnitinReport;
                getPrice();
                break;
            case R.id.rl_email:
                imgSendEmail.setImageDrawable(ContextCompat.getDrawable(this,
                        isSendEmail ? R.drawable.radio_unselect : R.drawable.radio_selected));
                isSendEmail = !isSendEmail;
                getPrice();
                break;
            case R.id.tv_apply:
                if (TextUtils.isEmpty(etCouponcode.getText().toString())) {
                    Toast.makeText(this, "enter coupon code", Toast.LENGTH_SHORT).show();
                    return;
                }
                applyCouponCode(etCouponcode.getText().toString());
                break;
            case R.id.img_coupon_close:
                tvHaveCode.setVisibility(View.VISIBLE);
                llCouponCode.setVisibility(View.GONE);
                etCouponcode.setText("");
                break;
            case R.id.tv_coupon_remove:
                removeCouponCode();
                break;
            case R.id.rl_save_order:
                if (isLogin()) {
                    if (isValid())
                        saveOrder();
                } else {
                    goToLoginSignup(true);
                }
                break;
            case R.id.tv_have_code:
                tvHaveCode.setVisibility(View.GONE);
                llCouponCode.setVisibility(View.VISIBLE);
                break;
            case R.id.ll_show_option:
                if (llMore.getVisibility() == View.GONE) {
                    llMore.setVisibility(View.VISIBLE);
                    imgArrow.setRotation(180);
                    tvMoreOptions.setText(getString(R.string.hide_more_options));
                } else {
                    llMore.setVisibility(View.GONE);
                    imgArrow.setRotation(0);
                    tvMoreOptions.setText(getString(R.string.show_more_options));
                }
                break;
            case R.id.tv_paper_type:
                showItemSelectDialog(Constants.PAPER_TYPES);
                break;
            case R.id.tv_subject:
                showItemSelectDialog(Constants.SUBJECTS_TYPES);
                break;
            case R.id.tv_format_style:
                showItemSelectDialog(Constants.FORMATED_STYLE_TYPES);
                break;
            case R.id.tv_discipline:
                showItemSelectDialog(Constants.DISCIPLIN_TYPES);
                break;
            case R.id.tv_preferred_writer:
                showItemSelectDialog(Constants.ACADEMIC_TYPES);
                break;
            case R.id.tv_category:
                showItemSelectDialog(Constants.CATEGORY_TYPES);
                break;
        }
    }

    public String getOtherSubject() {
        return etOtherSubject.getText().toString().trim();
    }

    public String getTopic() {
        return etTopic.getText().toString().trim();
    }

    public String getPrefferedWriter() {
        return tvPreferredWriter.getText().toString().trim();
    }

    public String getDetails() {
        return etDetails.getText().toString().trim();
    }

    public boolean isValid() {
        if (getSubject().equalsIgnoreCase("other") && isEmpty(getOtherSubject())) {
            validationError("Please enter other subject");
            return false;
        }

        if (isEmpty(getTopic())) {
            validationError("Please enter topic");
            return false;
        }

        if (isEmpty(getDetails())) {
            validationError("Please enter details(3 words atleast)");
            return false;
        }

        String[] split = getDetails().split(" ");
        if (split.length < 3) {
            validationError("Please enter atleast 3 words in details");
            return false;
        }

        return true;
    }

    public void applyCouponCode(final String couponCode) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<CouponCode> call = getService().applyCode(couponCode, getOrderId(), getAccessToken());
        call.enqueue(new Callback<CouponCode>() {
            @Override
            public void onResponse(Call<CouponCode> call, Response<CouponCode> response) {
                CouponCode model = response.body();
                if (checkStatus(model)) {
                    llCouponCode.setVisibility(View.GONE);
                    rlApplyingCode.setVisibility(View.VISIBLE);
                    tvOldTotal.setVisibility(View.VISIBLE);
                    tvCouponCode.setText(couponCode);
                    tvOldTotal.setText("$" + Utils.numberFormat(model.data.subTotal));
                    tvToolbarPrice.setText("$" + Utils.numberFormat(model.data.total));
                    tvTotal.setText("$" + Utils.numberFormat(model.data.total));
                } else {
                    if (!model.msg.equalsIgnoreCase(getString(R.string.invalid_access_token))) {
                        Toast.makeText(NewOrderActivity.this, model.msg, Toast.LENGTH_SHORT).show();
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<CouponCode> call, Throwable t) {
                failureError("get code failed");
            }
        });
    }

    public void removeCouponCode() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<CouponCode> call = getService().removeCode(getOrderId(), getAccessToken());
        call.enqueue(new Callback<CouponCode>() {
            @Override
            public void onResponse(Call<CouponCode> call, Response<CouponCode> response) {
                CouponCode model = response.body();
                if (checkStatus(model)) {
                    tvOldTotal.setVisibility(View.GONE);
                    tvToolbarPrice.setText("$" + Utils.numberFormat(model.data.total));
                    tvTotal.setText("$" + Utils.numberFormat(model.data.total));
                    rlApplyingCode.setVisibility(View.GONE);
                    tvHaveCode.setVisibility(View.VISIBLE);
                    etCouponcode.setText("");
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<CouponCode> call, Throwable t) {
                failureError("remove code failed");
            }
        });
    }

    public void saveOrder() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<SaveOrder> call = getService().saveOrder(getUserId(), getAccessToken(), getOrderId());
        call.enqueue(new Callback<SaveOrder>() {
            @Override
            public void onResponse(Call<SaveOrder> call, Response<SaveOrder> response) {
                SaveOrder model = response.body();
                if (checkStatus(model)) {
                    Preferences.writeString(NewOrderActivity.this, Constants.ORDER_ID, "");
                    getOrderById(model.data.orderid);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<SaveOrder> call, Throwable t) {
                failureError("save order failed");
            }
        });
    }

    public void getOrderById(String orderId) {
        if (!isNetworkConnected()) {
            return;
        }

        showProgress();

        Call<OrderByIdModel> call = getService().getOrderById(getAccessToken(), getUserId(), orderId);
        call.enqueue(new Callback<OrderByIdModel>() {
            @Override
            public void onResponse(Call<OrderByIdModel> call, Response<OrderByIdModel> response) {
                OrderByIdModel orderModel = response.body();
                if (checkStatus(orderModel)) {
                    Intent i = new Intent(NewOrderActivity.this, CheckoutActivity.class);
                    i.putExtra(Constants.ORDER_DATA, orderModel.data);
                    startActivity(i);
                    finish();
                    openToTop();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<OrderByIdModel> call, Throwable t) {
                failureError("No data found");
            }
        });
    }

    public void selectFileDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_camera_document_select);
        dialog.setCancelable(true);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.btn_cancel);
        LinearLayout llCamera = dialog.findViewById(R.id.ll_camera);
        LinearLayout llDocument = dialog.findViewById(R.id.ll_document);

        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission(false);
                dialog.dismiss();
            }
        });

        llDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission(true);
                dialog.dismiss();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void checkPermission(final boolean isDocument) {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            if (isDocument) {
                                Intent intent = new Intent(NewOrderActivity.this, NormalFilePickActivity.class);
                                intent.putExtra(Constant.MAX_NUMBER, 1);
                                intent.putExtra(NormalFilePickActivity.SUFFIX, new String[] {"xlsx", "xls", "doc", "docx", "ppt", "pptx", "pdf"});
                                startActivityForResult(intent, Constant.REQUEST_CODE_PICK_FILE);
                            } else {
                                Intent intent = new Intent(NewOrderActivity.this, ImagePickActivity.class);
                                intent.putExtra(IS_NEED_CAMERA, true);
                                intent.putExtra(Constant.MAX_NUMBER, 1);
                                startActivityForResult(intent, Constant.REQUEST_CODE_PICK_IMAGE);
                            }
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            Toast.makeText(NewOrderActivity.this, "Please give permission", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constant.REQUEST_CODE_PICK_FILE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    ArrayList<NormalFile> docPaths = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                    if (docPaths != null && docPaths.size() > 0) {
                        Log.e("Doc Path == > ", docPaths.get(0).getPath());
                        uploadFile(docPaths.get(0).getPath());
                    } else {
                        Toast.makeText(this, "File not selected", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case Constant.REQUEST_CODE_PICK_IMAGE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    ArrayList<ImageFile> imgPath = data.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
                    if (imgPath != null && imgPath.size() > 0) {
                        Log.e("Image Path == > ", imgPath.get(0).getPath());
                        uploadFile(imgPath.get(0).getPath());
                    } else {
                        Toast.makeText(this, "File not selected", Toast.LENGTH_SHORT).show();
                    }
                }
        }
    }

    public void uploadFile(String path) {
        if (!isNetworkConnected())
            return;

        showProgress();

        File file = new File(path);
        Uri selectedUri = Uri.fromFile(file);
        String fileExtension = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);

        RequestBody requestFile = null;
        if (mimeType != null) {
            requestFile = RequestBody.create(MediaType.parse(mimeType), file);
        }

        MultipartBody.Part body = null;
        if (requestFile != null) {
            body = MultipartBody.Part.createFormData("qqfile", file.getName(), requestFile);
        }

        RequestBody category = RequestBody.create(MultipartBody.FORM, tvCategory.getText().toString());
        RequestBody order_id = RequestBody.create(MultipartBody.FORM, getOrderId());
        RequestBody user_id = RequestBody.create(MultipartBody.FORM, getUserId());

        Log.e("Upload file Url = > ", Constants.BASE_URL + Constants.FILE_UPLOAD);
        Log.e("Params => ", "qqfile : " + file.getName() + "\n" + "category : " + tvCategory.getText().toString());

        Call<FileUpload> call = getService().uploadFile(body, category, order_id, user_id);
        call.enqueue(new Callback<FileUpload>() {
            @Override
            public void onResponse(Call<FileUpload> call, Response<FileUpload> response) {
                FileUpload fileUpload = response.body();
                if (checkStatus(fileUpload)) {
                    setFileAdapter(fileUpload.data);
                }

                hideProgress();
            }

            @Override
            public void onFailure(Call<FileUpload> call, Throwable t) {
                failureError("file upload failed");
            }
        });
    }

    public void setFileAdapter(List<FileUpload.Data> uploadedfiles) {
        if (uploadedfiles != null && uploadedfiles.size() > 0) {
            tvFilesCount.setText(uploadedfiles.size() + " Files");

            rvFiles.setVisibility(View.VISIBLE);
            tvNoFiles.setVisibility(View.GONE);

            if (fileAdapter == null) {
                fileAdapter = new UploadFileAdapter(NewOrderActivity.this);
            }

            fileAdapter.doRefresh(uploadedfiles);

            if (rvFiles.getAdapter() == null) {
                rvFiles.setAdapter(fileAdapter);
            }
        } else {
            tvFilesCount.setText("0 Files");
            rvFiles.setVisibility(View.GONE);
            tvNoFiles.setVisibility(View.VISIBLE);
        }
    }

    public void showItemSelectDialog(final String types) {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_item_select);
        dialog.setCancelable(true);

        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvApply = dialog.findViewById(R.id.tv_apply);
        final EditText etSearch = dialog.findViewById(R.id.et_search);
        RecyclerView rvTypes = dialog.findViewById(R.id.rv_items);

        switch (types) {
            case Constants.PAPER_TYPES:
                etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.type_of_paper)));
                break;
            case Constants.ACADEMIC_TYPES:
                etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.preferred_writer)));
                break;
            case Constants.SUBJECTS_TYPES:
                etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.subject)));
                break;
            case Constants.DISCIPLIN_TYPES:
                etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.discipline)));
                break;
            case Constants.FORMATED_STYLE_TYPES:
                etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.format_style)));
                break;
            case Constants.CATEGORY_TYPES:
                etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.category)));
                break;
        }

        rvTypes.setLayoutManager(new LinearLayoutManager(this));
        List<TypesModel.Data> mData = Preferences.getTypes(this, types);
        if (mData != null && mData.size() > 0) {
            for (int i = 0; i < mData.size(); i++) {
                switch (types) {
                    case Constants.PAPER_TYPES:
                        if (mData.get(i).paperName.equalsIgnoreCase(getPaperName())) {
                            mData.get(i).isSelected = true;
                        }
                        break;
                    case Constants.ACADEMIC_TYPES:
                        if (mData.get(i).academicLevelName.equalsIgnoreCase(getPrefferedWriter())) {
                            mData.get(i).isSelected = true;
                        }
                        break;
                    case Constants.SUBJECTS_TYPES:
                        if (mData.get(i).subjectName.equalsIgnoreCase(getSubject())) {
                            mData.get(i).isSelected = true;
                        }
                        break;
                    case Constants.DISCIPLIN_TYPES:
                        if (mData.get(i).discipline.equalsIgnoreCase(getDisciplinName())) {
                            mData.get(i).isSelected = true;
                        }
                        break;
                    case Constants.FORMATED_STYLE_TYPES:
                        if (mData.get(i).styleName.equalsIgnoreCase(getFormatName())) {
                            mData.get(i).isSelected = true;
                        }
                        break;
                    case Constants.CATEGORY_TYPES:
                        if (mData.get(i).category.equalsIgnoreCase(getCategory())) {
                            mData.get(i).isSelected = true;
                        }
                        break;
                }
            }
            typesAdapter = new TypesAdapter(this, mData, types);
            rvTypes.setAdapter(typesAdapter);
        }

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard(NewOrderActivity.this);
                dialog.dismiss();
            }
        });

        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard(NewOrderActivity.this);
                if (typesAdapter != null && typesAdapter.getSelectedItem() != null) {
                    switch (types) {
                        case Constants.PAPER_TYPES:
                            paperTypeId = typesAdapter.getSelectedItem().paperId;
                            tvPaperType.setText(typesAdapter.getSelectedItem().paperName);
                            if (getPaperName().equalsIgnoreCase("Others") || getPaperName().equalsIgnoreCase("Other")) {
                                etOtherPaper.setVisibility(View.VISIBLE);
                            } else {
                                etOtherPaper.setText("");
                                etOtherPaper.setVisibility(View.GONE);
                            }
                            getPrice();
                            break;
                        case Constants.ACADEMIC_TYPES:
                            prefferedId = typesAdapter.getSelectedItem().academicLevelId;
                            tvPreferredWriter.setText(typesAdapter.getSelectedItem().academicLevelName);
                            if (tvPreferredWriter.getText().toString().equalsIgnoreCase(getString(R.string.my_old_writer))) {
                                etOtherWriter.setVisibility(View.VISIBLE);
                            } else {
                                etOtherWriter.setText("");
                                etOtherWriter.setVisibility(View.GONE);
                            }
                            getPrice();
                            break;
                        case Constants.SUBJECTS_TYPES:
                            subjectId = typesAdapter.getSelectedItem().id;
                            tvSubject.setText(typesAdapter.getSelectedItem().subjectName);
                            if (getSubject().equalsIgnoreCase("Others") || getSubject().equalsIgnoreCase("Other")) {
                                etOtherSubject.setVisibility(View.VISIBLE);
                            } else {
                                etOtherSubject.setText("");
                                etOtherSubject.setVisibility(View.GONE);
                            }
                            getPrice();
                            break;
                        case Constants.DISCIPLIN_TYPES:
                            disciplineId = typesAdapter.getSelectedItem().disciplineId;
                            tvDiscipline.setText(typesAdapter.getSelectedItem().discipline);
                            getPrice();
                            break;
                        case Constants.FORMATED_STYLE_TYPES:
                            formatStyleId = typesAdapter.getSelectedItem().styleId;
                            tvFormatStyle.setText(typesAdapter.getSelectedItem().styleName);
                            if (getFormatName().equalsIgnoreCase("Others") || getFormatName().equalsIgnoreCase("Other")) {
                                etOtherFormat.setVisibility(View.VISIBLE);
                            } else {
                                etOtherFormat.setText("");
                                etOtherFormat.setVisibility(View.GONE);
                            }
                            getPrice();
                            break;
                        case Constants.CATEGORY_TYPES:
                            tvCategory.setText(typesAdapter.getSelectedItem().category);
                            break;
                    }
                }
                dialog.dismiss();
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (typesAdapter != null) {
                    typesAdapter.getFilter().filter(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etSearch.post(new Runnable() {
                    @Override
                    public void run() {
                        Utils.openSoftKeyboard(NewOrderActivity.this, etSearch);
                    }
                });
            }
        });
        etSearch.requestFocus();
    }

    public String getPaperName() {
        return tvPaperType.getText().toString();
    }

    public String getSubject() {
        return tvSubject.getText().toString();
    }

    public String getDisciplinName() {
        return tvDiscipline.getText().toString();
    }

    public String getFormatName() {
        return tvFormatStyle.getText().toString();
    }

    public String getCategory() {
        return tvCategory.getText().toString();
    }

    public void unselectServiceTypeAll() {
        btnWriting.setBackground(ContextCompat.getDrawable(this, R.drawable.left_corner_unselect));
        btnPowerpoint.setBackground(ContextCompat.getDrawable(this, R.drawable.right_corner_unselect));
        btnEditing.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        blackTextView(btnWriting, btnEditing, btnPowerpoint);
    }

    public void unselectWriterAll() {
        btnCollege.setBackground(ContextCompat.getDrawable(this, R.drawable.left_corner_unselect));
        btnMaster.setBackground(ContextCompat.getDrawable(this, R.drawable.right_corner_unselect));
        btnBachelor.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        blackTextView(btnCollege, btnBachelor, btnMaster);
    }

    public void unselectDeadlineAll() {
        tv3hours.setBackground(ContextCompat.getDrawable(this, R.drawable.left_corner_unselect));
        tv15days.setBackground(ContextCompat.getDrawable(this, R.drawable.right_corner_unselect));
        whiteBackgroundView(tv6hours, tv12hours, tv24hours, tv2days, tv4days, tv7days, tv10days);
        initDeadlineViews();
    }

    public void initDeadlineViews() {
        setTextAndColor(tv3hours, "3", getString(R.string.hours));
        setTextAndColor(tv6hours, "6", getString(R.string.hours));
        setTextAndColor(tv12hours, "12", getString(R.string.hours));
        setTextAndColor(tv24hours, "24", getString(R.string.hours));
        setTextAndColor(tv2days, "2", getString(R.string.days));
        setTextAndColor(tv4days, "4", getString(R.string.days));
        setTextAndColor(tv7days, "7", getString(R.string.days));
        setTextAndColor(tv10days, "10", getString(R.string.days));
        setTextAndColor(tv15days, "15", getString(R.string.days));
    }

    public void setTextAndColor(TextView textView, String text, String time) {
        String htmltext;
        if (time.equals("Hours")) {
            htmltext = "<font color=black><big>" + text + "</big></font><br><font color='#D0555A'><small>" + time + "</small></font>";
        } else {
            htmltext = "<font color=black><big>" + text + "</big></font><br><font color='#13B675'><small>" + time + "</small></font>";
        }

        textView.setText(Utils.fromHtml(htmltext));
    }

    public void selectedView(TextView textView, String text, String time) {
        switch (text) {
            case "3":
                textView.setBackground(ContextCompat.getDrawable(this, R.drawable.left_corner_select));
                break;
            case "15":
                textView.setBackground(ContextCompat.getDrawable(this, R.drawable.right_corner_select));
                break;
            default:
                textView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                break;
        }
        String htmltext = "<font color='#ffffff'><big>" + text + "</big></font><br><font color='#ffffff'><small>" + time + "</small></font>";
        textView.setText(Utils.fromHtml(htmltext));

        getPrice();
    }

    public Dialog onCreateDialogSingleChoice() {
        //ContextThemeWrapper cw = new ContextThemeWrapper(this, R.style.AlertDialogTheme);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final CharSequence[] array = {"Single Spaced = 560 words per Page", "Double Spaced = 280 words per Page"};
        builder.setItems(array, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (array[which].toString().contains("Single")) {
                    spacing = "2";
                } else {
                    spacing = "1";
                }
                tvSpacedPage.setText(array[which].toString().split("=")[0].trim());
                tvWords.setText(array[which].toString().replaceAll("[\\D]", "") + " words");
                dialog.dismiss();
            }
        });
        return builder.create();
    }

    public void deleteAllFile() {
        if (!isNetworkConnected())
            return;

        Call call = getService().deleteAllFiles();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                setFileAdapter(null);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                failureError("delete all files failed");
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Preferences.writeString(NewOrderActivity.this, Constants.ORDER_ID, "");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToBottom();
    }
}
