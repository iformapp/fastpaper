package com.fastpaper.ui.order;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.util.Attributes;
import com.fastpaper.R;
import com.fastpaper.adapter.OrderAdapter;
import com.fastpaper.adapter.OrderAdapter.EditOrderListner;
import com.fastpaper.model.OrderModel;
import com.fastpaper.ui.BaseActivity;
import com.fastpaper.ui.MainActivity;
import com.fastpaper.util.Constants;
import com.fastpaper.util.Constants.FilterType;
import com.fastpaper.util.Utils;
import com.fastpaper.util.textview.TextViewSFDisplayBold;
import com.fastpaper.util.textview.TextViewSFDisplayRegular;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderActivity extends BaseActivity implements View.OnClickListener, EditOrderListner {

    @BindView(R.id.tvTitle)
    TextViewSFDisplayBold tvTitle;
    @BindView(R.id.tvInfo)
    TextViewSFDisplayRegular tvInfo;
    @BindView(R.id.img_delete)
    ImageView imgDelete;
    @BindView(R.id.img_filter)
    ImageView imgFilter;
    @BindView(R.id.rv_orders)
    RecyclerView rvOrders;
    @BindView(R.id.rl_without_login)
    RelativeLayout rlWithoutLogin;
    @BindView(R.id.ll_order_view)
    LinearLayout llOrderView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private List<OrderModel.Data> orderArrayList;
    private OrderAdapter mAdapter;
    private boolean isDeleteSelected = false;
    private boolean isFilterSelected = false;
    private Dialog dialog;
    private TextView tvAll;
    private TextView tvCurrent;
    private TextView tvUnpaid;
    private TextView tvCompleted;
    private TextView tvRefunded;
    private TextView tvCancel;
    private TextView tvApply;
    private String filterType = FilterType.ALL.getValue();
    private boolean isPullToRefresh = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        ButterKnife.bind(this);
    }

    public void init() {
        filterType = FilterType.ALL.getValue();
        imgFilter.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.filter));
        orderArrayList = new ArrayList<>();
        rvOrders.setLayoutManager(new LinearLayoutManager(this));
        getOrders(filterType);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isPullToRefresh = true;
                getOrders(filterType);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isLogin()) {
            llOrderView.setVisibility(View.VISIBLE);
            rlWithoutLogin.setVisibility(View.GONE);

            if (orderArrayList == null || orderArrayList.isEmpty()) {
                init();
            }
        } else {
            tvTitle.setText(getString(R.string.my_orders));
            tvInfo.setText(getString(R.string.login_to_orders));
            llOrderView.setVisibility(View.GONE);
            rlWithoutLogin.setVisibility(View.VISIBLE);
        }
    }

    public void getOrders(String stat) {
        if (!isNetworkConnected()) {
            if (swipeRefreshLayout != null) {
                swipeRefreshLayout.setRefreshing(false);
            }
            return;
        }

        if (!isPullToRefresh)
            showProgress();

        Call<OrderModel> orderCall = getService().getOrder(getAccessToken(), getUserId(), stat);
        orderCall.enqueue(new Callback<OrderModel>() {
            @Override
            public void onResponse(Call<OrderModel> call, Response<OrderModel> response) {
                isPullToRefresh = false;
                OrderModel orderModel = response.body();
                if (checkStatus(orderModel)) {
                    orderArrayList = new ArrayList<>();
                    orderArrayList = orderModel.data;
                } else {
                    orderArrayList = new ArrayList<>();
                }
                setAdapter();
                hideProgress();
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<OrderModel> call, Throwable t) {
                failureError("get Order failed");
                isPullToRefresh = false;
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    public void setAdapter() {
        if (orderArrayList != null && orderArrayList.size() > 0) {
            if (imgDelete != null) {
                imgDelete.setVisibility(View.VISIBLE);
            }
            if (imgFilter != null) {
                imgFilter.setVisibility(View.VISIBLE);
            }
            if (mAdapter == null) {
                mAdapter = new OrderAdapter(this);
                mAdapter.setEditOrderListner(this);
                mAdapter.setMode(Attributes.Mode.Single);
            }

            mAdapter.doRefresh(orderArrayList);

            if (rvOrders != null && rvOrders.getAdapter() == null) {
                rvOrders.setAdapter(mAdapter);
            }
        } else {
            if (mAdapter != null) {
                mAdapter.doRefresh(null);
            }
            Toast.makeText(this, "No orders found", Toast.LENGTH_SHORT).show();
        }
    }

    public void showFilterDialog() {
        dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_filter);
        dialog.setCancelable(true);

        tvAll = (TextView) dialog.findViewById(R.id.tv_all);
        tvCurrent = (TextView) dialog.findViewById(R.id.tv_current);
        tvUnpaid = (TextView) dialog.findViewById(R.id.tv_unpaid);
        tvCompleted = (TextView) dialog.findViewById(R.id.tv_completed);
        tvRefunded = (TextView) dialog.findViewById(R.id.tv_refunded);
        tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        tvApply = (TextView) dialog.findViewById(R.id.tv_apply);

        tvAll.setOnClickListener(this);
        tvCurrent.setOnClickListener(this);
        tvUnpaid.setOnClickListener(this);
        tvCompleted.setOnClickListener(this);
        tvRefunded.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvApply.setOnClickListener(this);

        clearViews();
        switch (filterType) {
            case "all":
                setSelectedView(tvAll);
                break;
            case "current":
                setSelectedView(tvCurrent);
                break;
            case "unpaid":
                setSelectedView(tvUnpaid);
                break;
            case "completed":
                setSelectedView(tvCompleted);
                break;
            case "refunded":
                setSelectedView(tvRefunded);
                break;
        }

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.TOP;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_all:
                clearViews();
                setSelectedView(tvAll);
                filterType = FilterType.ALL.getValue();
                break;
            case R.id.tv_current:
                clearViews();
                setSelectedView(tvCurrent);
                filterType = FilterType.CURRENT.getValue();
                break;
            case R.id.tv_unpaid:
                clearViews();
                setSelectedView(tvUnpaid);
                filterType = FilterType.UNPAID.getValue();
                break;
            case R.id.tv_completed:
                clearViews();
                setSelectedView(tvCompleted);
                filterType = FilterType.COMPLETED.getValue();
                break;
            case R.id.tv_refunded:
                clearViews();
                setSelectedView(tvRefunded);
                filterType = FilterType.REFUNDED.getValue();
                break;
            case R.id.tv_cancel:
                dialog.dismiss();
                break;
            case R.id.tv_apply:
                if (filterType.equals(FilterType.ALL.getValue())) {
                    imgFilter.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.filter));
                } else {
                    imgFilter.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.filter_selected));
                }
                dialog.dismiss();
                getOrders(filterType);
                break;
        }
    }

    public void clearViews() {
        setInvalidateViews(tvAll, tvCompleted, tvCurrent, tvRefunded, tvUnpaid);
    }

    public void setSelectedView(View v) {
        v.setBackground(ContextCompat.getDrawable(this, R.drawable.filter_selected_bg));
        ((TextView) v).setTextColor(Color.WHITE);
    }

    public void setInvalidateViews(View... views) {
        for (View v : views) {
            v.setBackground(ContextCompat.getDrawable(this, R.drawable.filter_unselect_bg));
            ((TextView) v).setTextColor(Color.BLACK);
        }
    }

    @OnClick({R.id.btn_signup, R.id.btn_login, R.id.img_delete, R.id.img_filter})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_signup:
                if (getParent() != null) {
                    ((MainActivity) getParent()).goToLoginSignup(false);
                } else {
                    goToLoginSignup(false);
                }
                break;
            case R.id.btn_login:
                if (getParent() != null) {
                    ((MainActivity) getParent()).goToLoginSignup(true);
                } else {
                    goToLoginSignup(true);
                }
                break;
            case R.id.img_delete:
                setToggleDelete(isDeleteSelected);
                break;
            case R.id.img_filter:
                showFilterDialog();
                break;
        }
    }

    public void setToggleDelete(boolean isDeleteSelected) {
        if (orderArrayList != null && orderArrayList.size() > 0) {
            if (isDeleteSelected) {
                if (mAdapter != null)
                    mAdapter.mItemManger.closeItem(0);
            } else {
                if (mAdapter != null)
                    mAdapter.mItemManger.openItem(0);
            }
            imgDelete.setImageDrawable(ContextCompat.getDrawable(this,
                    isDeleteSelected ? R.drawable.delete : R.drawable.delete_selected));
            this.isDeleteSelected = !isDeleteSelected;
        }
    }

    public void setToggleDeleteOff() {
        if (orderArrayList != null && orderArrayList.size() > 0) {
            imgDelete.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.delete));
            isDeleteSelected = false;
        }
    }

    @Override
    public void onEditOrder() {
        showEditDialog();
    }

    public void showEditDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_Dialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_edit_order);
        dialog.setCancelable(true);

        TextView tvMessage = (TextView) dialog.findViewById(R.id.tv_message);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        TextView tvChatnow = (TextView) dialog.findViewById(R.id.tv_chat_now);

        tvMessage.setText(Utils.fromHtml(getString(R.string.edit_order_text)));

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvChatnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intercom.client().displayMessenger();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }
}
