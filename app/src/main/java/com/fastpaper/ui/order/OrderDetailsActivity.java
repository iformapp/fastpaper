package com.fastpaper.ui.order;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.fastpaper.R;
import com.fastpaper.fragment.OrderDetailsFragment;
import com.fastpaper.fragment.OrderFilesFragment;
import com.fastpaper.fragment.OrderPaymentFragment;
import com.fastpaper.fragment.OrderStatusFragment;
import com.fastpaper.listener.OnFilePick;
import com.fastpaper.model.OrderByIdModel;
import com.fastpaper.model.OrderModel;
import com.fastpaper.ui.BaseActivity;
import com.fastpaper.util.Constants;
import com.fastpaper.util.textview.TextViewSFDisplayBold;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.filter.entity.ImageFile;
import com.vincent.filepicker.filter.entity.NormalFile;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsActivity extends BaseActivity {

    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.tv_order_no)
    TextViewSFDisplayBold tvOrderNo;

    private OrderByIdModel.Data orderData;
    private OrderModel.Data singleData;
    private boolean isRefresh = false;
    private OnFilePick onFilePick;

    private String[] tabTexts = {"Status", "Details", "Files", "Payment"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            singleData = (OrderModel.Data) getIntent().getSerializableExtra(Constants.ORDER_DATA);
        }
        if (singleData == null)
            return;

        tvOrderNo.setText(singleData.orderId);
        getOrderById(singleData.orderId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isRefresh) {
            isRefresh = false;
            getOrderById(singleData.orderId);
        }
    }

    public void setupTabs() {
        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                Typeface tf = Typeface.createFromAsset(getAssets(), Constants.SFDISPLAY_BOLD);
                if (text != null) {
                    text.setTypeface(tf);
                    text.setTextColor(Color.BLACK);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                Typeface tf = Typeface.createFromAsset(getAssets(), Constants.SFDISPLAY_REGULAR);
                if (text != null) {
                    text.setTypeface(tf);
                    text.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.colorAccent));
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setupTabText();
    }

    public void getOrderById(String orderId) {
        if (!isNetworkConnected()) {
            return;
        }

        showProgress();

        Call<OrderByIdModel> call = getService().getOrderById(getAccessToken(), getUserId(), orderId);
        call.enqueue(new Callback<OrderByIdModel>() {
            @Override
            public void onResponse(Call<OrderByIdModel> call, Response<OrderByIdModel> response) {
                OrderByIdModel orderModel = response.body();
                if (checkStatus(orderModel)) {
                    orderData = orderModel.data;
                    setupTabs();
                } else {
                    isRefresh = true;
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<OrderByIdModel> call, Throwable t) {
                failureError("No data found");
            }
        });
    }

    public OrderByIdModel.Data getOrderData() {
        return orderData;
    }

    public void setOrderData(OrderByIdModel.Data orderData) {
        this.orderData =  orderData;
    }

    private void setupTabText() {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TextView tv = (TextView) LayoutInflater.from(this)
                    .inflate(i == 0 ? R.layout.custom_tab_text_select : R.layout.custom_tab_text_unselect, null);
            tv.setText(tabTexts[i]);
            tabLayout.getTabAt(i).setCustomView(tv);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(OrderStatusFragment.newInstanace(singleData), getString(R.string.status));
        adapter.addFrag(OrderDetailsFragment.newInstanace(false), getString(R.string.details));
        adapter.addFrag(OrderFilesFragment.newInstanace(false), getString(R.string.files));
        adapter.addFrag(OrderPaymentFragment.newInstanace(singleData), getString(R.string.payment));
        viewPager.setAdapter(adapter);

        viewPager.setOffscreenPageLimit(3);
    }

    @OnClick({R.id.ll_back, R.id.tv_chat})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_chat:
                Intercom.client().displayMessenger();
                break;
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToBottom();
    }

    public void setOnFilePickListener(OnFilePick onFilePick) {
        this.onFilePick = onFilePick;
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        switch (requestCode) {
//            case Constant.REQUEST_CODE_PICK_FILE:
//                if (resultCode == Activity.RESULT_OK && data != null) {
//                    ArrayList<NormalFile> docPaths = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
//                    if (docPaths != null && docPaths.size() > 0) {
//                        Log.e("Doc Path == > ", docPaths.get(0).getPath());
//                        if (onFilePick != null)
//                            onFilePick.filePick(docPaths.get(0).getPath());
//                    } else {
//                        Toast.makeText(this, "File not selected", Toast.LENGTH_SHORT).show();
//                    }
//                }
//                break;
//            case Constant.REQUEST_CODE_PICK_IMAGE:
//                if (resultCode == Activity.RESULT_OK && data != null) {
//                    ArrayList<ImageFile> imgPath = data.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
//                    if (imgPath != null && imgPath.size() > 0) {
//                        Log.e("Image Path == > ", imgPath.get(0).getPath());
//                        if (onFilePick != null)
//                            onFilePick.filePick(imgPath.get(0).getPath());
//                    } else {
//                        Toast.makeText(this, "File not selected", Toast.LENGTH_SHORT).show();
//                    }
//                }
//                break;
//        }
//    }
}
