package com.fastpaper.ui.policy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.fastpaper.R;
import com.fastpaper.adapter.RecyclerviewAdapter;
import com.fastpaper.ui.BaseActivity;
import com.fastpaper.util.Constants;
import com.fastpaper.util.textview.TextViewSFDisplayBold;
import com.fastpaper.util.textview.TextViewSFDisplayRegular;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;

public class PolicyActivity extends BaseActivity implements RecyclerviewAdapter.OnViewBindListner {

    @BindView(R.id.tv_toolbar_title)
    TextViewSFDisplayBold tvToolbarTitle;
    @BindView(R.id.tv_right)
    TextViewSFDisplayRegular tvRight;
    @BindView(R.id.rv_policy)
    RecyclerView rvPolicy;

    private RecyclerviewAdapter mAdapter;
    private ArrayList<String> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy);
        ButterKnife.bind(this);

        arrayList = fillData();

        tvToolbarTitle.setText(getString(R.string.policy).toUpperCase());
        tvRight.setText(getString(R.string.chat));

        rvPolicy.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new RecyclerviewAdapter(arrayList, R.layout.item_policy, this);
        rvPolicy.setAdapter(mAdapter);
    }

    @OnClick({R.id.ll_back, R.id.tv_right})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_right:
                Intercom.client().displayMessenger();
                break;
        }
    }

    @Override
    public void bindView(View view, final int position) {
        TextView textView = view.findViewById(R.id.tv_policy_title);
        View itemView = view.findViewById(R.id.rl_view);
        textView.setText(arrayList.get(position));
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PolicyActivity.this, PolicyDetailActivity.class);
                switch (position) {
                    case 0:
                        i.putExtra(Constants.POLICY_URL, Constants.REFUND);
                        break;
                    case 1:
                        i.putExtra(Constants.POLICY_URL, Constants.PRIVACY);
                        break;
                    case 2:
                        i.putExtra(Constants.POLICY_URL, Constants.REVISION);
                        break;
                    case 3:
                        i.putExtra(Constants.POLICY_URL, Constants.TERMS_USE);
                        break;
                    case 4:
                        i.putExtra(Constants.POLICY_URL, Constants.DISCLAIMER);
                        break;
                    case 5:
                        i.putExtra(Constants.POLICY_URL, Constants.WEBSITE);
                        break;
                    case 6:
                        i.putExtra(Constants.POLICY_URL, Constants.FAQS);
                        break;
                    case 7:
                        i.putExtra(Constants.POLICY_URL, Constants.SERVICES);
                        break;
                    case 8:
                        i.putExtra(Constants.POLICY_URL, Constants.PRICES);
                        break;
                }
                startActivity(i);
                openToLeft();
            }
        });
    }

    public ArrayList fillData() {
        arrayList = new ArrayList<>();
        arrayList.add(getString(R.string.refund_policy));
        arrayList.add(getString(R.string.privacy_policy));
        arrayList.add(getString(R.string.revision_policy));
        arrayList.add(getString(R.string.terms_of_use));
        arrayList.add(getString(R.string.disclaimer));
        arrayList.add(getString(R.string.website));
        //arrayList.add(getString(R.string.about_us));
        arrayList.add(getString(R.string.faqs));
        arrayList.add(getString(R.string.services));
        arrayList.add(getString(R.string.prices));
        return arrayList;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
