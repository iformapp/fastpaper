package com.fastpaper.ui.policy;

import android.os.Bundle;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fastpaper.R;
import com.fastpaper.ui.BaseActivity;
import com.fastpaper.util.Constants;
import com.fastpaper.util.textview.TextViewSFDisplayBold;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PolicyDetailActivity extends BaseActivity {

    @BindView(R.id.tv_toolbar_title)
    TextViewSFDisplayBold tvToolbarTitle;
    @BindView(R.id.webView)
    WebView webView;

    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy_detail);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            url = getIntent().getStringExtra(Constants.POLICY_URL);
        }

        tvToolbarTitle.setText(getString(R.string.policy));

        showProgress();

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                hideProgress();
            }
        });

        webView.loadUrl(url);
    }

    @OnClick(R.id.ll_back)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
