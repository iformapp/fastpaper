package com.fastpaper.ui.profile;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.fastpaper.R;
import com.fastpaper.ccp.CountryCodePicker;
import com.fastpaper.model.UserModel;
import com.fastpaper.ui.BaseActivity;
import com.fastpaper.util.Preferences;
import com.fastpaper.util.edittext.EditTextSFTextRegular;
import com.fastpaper.util.textview.TextViewSFDisplayBold;
import com.fastpaper.util.textview.TextViewSFTextBold;
import com.fastpaper.util.textview.TextViewSFTextRegular;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends BaseActivity {

    @BindView(R.id.tv_toolbar_title)
    TextViewSFDisplayBold tvToolbarTitle;
    @BindView(R.id.btn_right)
    TextViewSFTextBold btnRight;
    @BindView(R.id.et_mobile)
    EditTextSFTextRegular etMobile;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.tv_phone_prefix)
    TextViewSFTextRegular tvPhonePrefix;
    @BindView(R.id.tv_email)
    TextViewSFTextRegular tvEmail;
    @BindView(R.id.et_firstname)
    EditTextSFTextRegular etFirstname;
    @BindView(R.id.et_lastname)
    EditTextSFTextRegular etLastname;

    private UserModel.Data userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        tvToolbarTitle.setText(getString(R.string.edit_profile));

        userData = Preferences.getUserData(this);
        if (userData == null) {
            finish();
            return;
        }

        etFirstname.setText(userData.firstName);
        etLastname.setText(userData.lastName);

        if (userData.email != null) {
            tvEmail.setText(userData.email);
        }

        if (userData.mobile != null) {
            etMobile.setText(userData.mobile);
            if (userData.dialcode != null && !isEmpty(userData.dialcode)) {
                String code = userData.dialcode.replace("+", "");
                int countryCode = Integer.parseInt(code);
                tvPhonePrefix.setText("+" + code);
                ccp.setCountryForPhoneCode(countryCode);
            }
        }

        addTextChangeEvent(etFirstname, etLastname, etMobile);

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                btnRight.setVisibility(View.VISIBLE);
                tvPhonePrefix.setText(ccp.getSelectedCountryCodeWithPlus());
            }
        });
    }

    public String getFirstName() {
        return etFirstname.getText().toString();
    }

    public String getLastName() {
        return etLastname.getText().toString();
    }

    public String getMobile() {
        return etMobile.getText().toString();
    }

    public String getMobilePrefix() {
        return ccp.getSelectedCountryCodeWithPlus();
    }

    public void saveProfile() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<UserModel> call = getService().saveProfile(getAccessToken(), getUserId(), getFirstName(), getLastName(),
                getMobile(), getMobilePrefix());
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                UserModel userModel = response.body();
                if (userModel != null) {
                    if (checkStatus(userModel)) {
                        if (userModel.data != null) {
                            Preferences.saveUserData(EditProfileActivity.this, userModel.data);
                            Toast.makeText(EditProfileActivity.this, userModel.msg, Toast.LENGTH_SHORT).show();
                            btnRight.setVisibility(View.GONE);
                        }
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                failureError("edit profile failed");
            }
        });
    }

    public void addTextChangeEvent(EditText... editTexts) {
        for (EditText edittext : editTexts) {
            edittext.addTextChangedListener(textWatcher);
        }
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (btnRight.getVisibility() == View.GONE) {
                btnRight.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @OnClick({R.id.ll_back, R.id.btn_right, R.id.tv_changepassword})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.btn_right:
                if (validData()) {
                    saveProfile();
                }
                break;
            case R.id.tv_changepassword:
                redirectActivity(UpdatePasswordActivity.class);
                break;
        }
    }

    public boolean validData() {
        if (isEmpty(getFirstName())) {
            validationError("Enter Name");
            return false;
        }

        if (isEmpty(getMobile())) {
            validationError("Enter Mobile no");
            return false;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
