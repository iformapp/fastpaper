package com.fastpaper.ui.profile;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fastpaper.R;
import com.fastpaper.model.GeneralModel;
import com.fastpaper.model.UserModel;
import com.fastpaper.ui.BaseActivity;
import com.fastpaper.ui.MainActivity;
import com.fastpaper.ui.policy.PolicyActivity;
import com.fastpaper.util.Constants;
import com.fastpaper.util.Preferences;
import com.fastpaper.util.textview.TextViewSFDisplayBold;
import com.fastpaper.util.textview.TextViewSFDisplayRegular;
import com.fastpaper.util.textview.TextViewSFTextBold;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends BaseActivity {

    @BindView(R.id.tv_balance)
    TextViewSFTextBold tvBalance;
    @BindView(R.id.tvTitle)
    TextViewSFDisplayBold tvTitle;
    @BindView(R.id.tvInfo)
    TextViewSFDisplayRegular tvInfo;
    @BindView(R.id.tv_username)
    TextViewSFDisplayBold tvUsername;
    @BindView(R.id.tv_email)
    TextViewSFDisplayRegular tvEmail;
    @BindView(R.id.ll_profile_view)
    LinearLayout llProfileView;
    @BindView(R.id.rl_without_login)
    RelativeLayout rlWithoutLogin;

    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isLogin()) {
            UserModel.Data userData = Preferences.getUserData(this);
            if (userData != null) {
                tvUsername.setText("Hi, " + userData.firstName);
                tvEmail.setText(userData.email);
                tvBalance.setText("$0.00");
            }
            llProfileView.setVisibility(View.VISIBLE);
            rlWithoutLogin.setVisibility(View.GONE);
        } else {
            tvTitle.setText(getString(R.string.profile));
            tvInfo.setText(getString(R.string.login_to_profile));
            llProfileView.setVisibility(View.GONE);
            rlWithoutLogin.setVisibility(View.VISIBLE);
        }
    }

    @OnClick({R.id.btn_signup, R.id.btn_login, R.id.rl_edit_profile, R.id.rl_my_balance, R.id.rl_policy, R.id.rl_feedback, R.id.rl_sign_out,
            R.id.rl_profile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_signup:
                if (getParent() != null) {
                    ((MainActivity) getParent()).goToLoginSignup(false);
                } else {
                    goToLoginSignup(false);
                }
                break;
            case R.id.btn_login:
                if (getParent() != null) {
                    ((MainActivity) getParent()).goToLoginSignup(true);
                } else {
                    goToLoginSignup(true);
                }
                break;
            case R.id.rl_profile:
            case R.id.rl_edit_profile:
                redirectActivity(EditProfileActivity.class);
                break;
            case R.id.rl_my_balance:
                break;
            case R.id.rl_policy:
                redirectActivity(PolicyActivity.class);
                break;
            case R.id.rl_feedback:
                showFeedbackDialog();
                break;
            case R.id.rl_sign_out:
                showLogoutDialog();
                break;
        }
    }

    private void showLogoutDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_Dialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_edit_order);
        dialog.setCancelable(true);

        TextView tvMessage = (TextView) dialog.findViewById(R.id.tv_message);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        TextView tvChatnow = (TextView) dialog.findViewById(R.id.tv_chat_now);

        tvMessage.setText(getString(R.string.logout_msg));

        tvCancel.setText(getString(R.string.no));
        tvChatnow.setText(getString(R.string.yes));
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvChatnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Toast.makeText(ProfileActivity.this, "Signout successfully", Toast.LENGTH_SHORT).show();
                Preferences.clearPreferences(ProfileActivity.this);
                Preferences.saveUserData(ProfileActivity.this, null);
                gotoMainActivity(Constants.TAB_PROFILE);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void showFeedbackDialog() {
        dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_feedback);
        dialog.setCancelable(true);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        TextView tvSend = (TextView) dialog.findViewById(R.id.tv_send);
        final EditText etFeedback = (EditText) dialog.findViewById(R.id.et_feedback);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isEmpty(etFeedback.getText().toString().trim())) {
                    sendFeedback(etFeedback.getText().toString());
                    dialog.dismiss();
                } else {
                    Toast.makeText(ProfileActivity.this, "Enter message", Toast.LENGTH_SHORT).show();
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void sendFeedback(String message) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().sendFeedback(getUserId(), message);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (checkStatus(response.body())) {
                        Toast.makeText(ProfileActivity.this, response.body().msg, Toast.LENGTH_SHORT).show();
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("update password failed");
            }
        });
    }
}
