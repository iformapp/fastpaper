package com.fastpaper.ui.profile;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.fastpaper.R;
import com.fastpaper.model.GeneralModel;
import com.fastpaper.ui.BaseActivity;
import com.fastpaper.util.edittext.EditTextSFDisplayRegular;
import com.fastpaper.util.textview.TextViewSFDisplayRegular;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdatePasswordActivity extends BaseActivity {

    @BindView(R.id.tv_right)
    TextViewSFDisplayRegular tvRight;
    @BindView(R.id.et_password)
    EditTextSFDisplayRegular etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);
        ButterKnife.bind(this);

        tvRight.setText(getString(R.string.chat));
    }

    public String getPassword() {
        return etPassword.getText().toString().trim();
    }

    public void savePassword() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().setPassword(getAccessToken(), getPassword());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (checkStatus(response.body())) {
                        Toast.makeText(UpdatePasswordActivity.this, "Password update successfully", Toast.LENGTH_SHORT).show();
                        etPassword.setText("");
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("update password failed");
            }
        });
    }

    @OnClick({R.id.ll_back, R.id.tv_right, R.id.tv_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_right:
                Intercom.client().displayMessenger();
                break;
            case R.id.tv_save:
                if (!isEmpty(getPassword())) {
                    savePassword();
                } else {
                    Toast.makeText(this, "Please enter valid password", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
