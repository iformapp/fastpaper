package com.fastpaper.util;

public class Constants {

    //API URL PARAMS
    public static final String BASE_URL = "https://www.fastpaper.com/";
    public static final String URL = "api/a-webservice.php";
    public static final String FILE_PATH = BASE_URL + "uploads/customers_files/";
    public static final String LOGIN = URL + "?method=android_login";
    public static final String REGISTER = URL + "?method=signup";
    public static final String SAVE_PROFILE = URL + "?method=saveProfile";
    public static final String SET_PASSWORD = URL + "?method=setPasswordOnLoggedin";
    public static final String SEND_FEEDBACK = URL + "?method=sendFeedback";
    public static final String GET_ORDER = URL + "?method=getOrders";
    public static final String DELETE_ORDER = URL + "?method=deleteOrder";
    public static final String ORDER_DETAIL = URL + "?method=getOrdersById";
    public static final String ORDER_UPDATE = URL + "?method=updateOrderStatus";
    public static final String FORGOT_PASSWORD = URL + "?method=forgotPassword";
    public static final String PRICE_CALCULATE = URL + "?method=android_SetOrderOptionAll";
    public static final String FILE_UPLOAD = URL + "?method=android_MyUploadMaterial";
    public static final String NEW_FILE_UPLOAD = URL + "?method=uploadNewFile";
    public static final String DELETE_UPLOAD = URL + "?method=android_MyDeleteMaterial";
    public static final String DELETE_ALL_FILES = URL + "?method=deleteUploadedFiles";
    public static final String ORDER_SAVE = URL + "?method=android_MyOrderSave";
    public static final String APPLY_COUPON = URL + "?method=android_UpdateDisc";
    public static final String REMOVE_COUPON = URL + "?method=android_MyRemoveDisc";

    public static final String REFUND = BASE_URL + "money-back-guarantee.php";
    public static final String PRIVACY = BASE_URL + "privacy- policy.php";
    public static final String REVISION = BASE_URL + "revision-policy.php";
    public static final String TERMS_USE = BASE_URL + "terms-of-use.php";
    public static final String DISCLAIMER = BASE_URL + "disclaimer.php";
    public static final String WEBSITE = BASE_URL + "index.php";
    public static final String FAQS = BASE_URL + "faqs.php";
    public static final String SERVICES = BASE_URL + "index.php#service-section";
    public static final String PRICES = BASE_URL + "price.php";

    //Api parameter
    public static final String PAPER_TYPES = "paperTypes";
    public static final String ACADEMIC_TYPES = "getAcademicTypes";
    public static final String DISCIPLIN_TYPES = "getDisciplines";
    public static final String FORMATED_STYLE_TYPES = "getFormatedStyle";
    public static final String SUBJECTS_TYPES = "subjects";
    public static final String CATEGORY_TYPES = "getCategory";

    public static final int TAB_HOME = 0;
    public static final int TAB_CHAT = 1;
    public static final int TAB_PLUS = 2;
    public static final int TAB_ORDER = 3;
    public static final int TAB_PROFILE = 4;

    public static final String SFTEXT_REGULAR = "font/SanFranciscoText-Regular.otf";
    public static final String SFTEXT_BOLD = "font/SanFranciscoText-Bold.otf";
    public static final String SFDISPLAY_BOLD = "font/SF-Pro-Display-Bold.otf";
    public static final String SFDISPLAY_REGULAR = "font/SF-Pro-Display-Regular.otf";

    public static final String FROM_LOGIN = "from login";
    public static final String IS_VISA = "isVisa";
    public static final String IS_LOGIN = "isLogin";
    public static final String FCM_TOKEN = "fcm_token";
    public static final String POLICY_URL = "policy_url";
    public static final String USER_DATA = "user_data";
    public static final String ORDER_DATA = "order_data";
    public static final String ORDER_ID = "order_id";
    public static final String SCREEN_NAME = "screen_name";
    public static final String DEADLINE_TYPE = "deadlineType";
    public static final String DEADLINE_VALUE = "deadlineValue";
    public static final String WRITER_LEVEL_ID = "writerLevelId";
    public static final String PAGE = "page";

    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final int COLLEGE_ID = 4;
    public static final int BECHELOR_ID = 5;
    public static final int MASTER_ID = 6;

    public static final int WRITING_ID = 4;
    public static final int EDITING_ID = 5;
    public static final int POWERPOINT_ID = 6;

    public static final int REQUEST_CODE_IMAGE = 23;

    public enum FilterType {
        ALL("all"),
        CURRENT("current"),
        UNPAID("unpaid"),
        COMPLETED("completed"),
        REFUNDED("refunded");

        String type;

        FilterType(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }
}
