package com.fastpaper.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.fastpaper.model.TypesModel;
import com.fastpaper.model.UserModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Preferences {

    private static final String PREF_NAME = "fastpaper";

    private static final int MODE = Context.MODE_PRIVATE;

    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).commit();
    }

    public static boolean readBoolean(Context context, String key, boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).commit();
    }

    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).commit();

    }

    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    public static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).commit();
    }

    public static float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    public static void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).commit();
    }

    public static long readLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }

    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    private static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

    public static void clearPreferences(Context context) {
        getEditor(context).clear().apply();
    }

    public static void saveTypes(Context context, List<TypesModel.Data> paperType, String types) {
        SharedPreferences mPrefs = context.getSharedPreferences(types, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString("myJson", new Gson().toJson(paperType));
        prefsEditor.apply();
    }

    public static List<TypesModel.Data> getTypes(Context context, String types) {
        List<TypesModel.Data> callLog;
        SharedPreferences mPrefs = context.getSharedPreferences(types, Context.MODE_PRIVATE);
        String json = mPrefs.getString("myJson", "");
        if (json.isEmpty()) {
            callLog = new ArrayList<>();
        } else {
            Type type = new TypeToken<List<TypesModel.Data>>() {
            }.getType();
            callLog = new Gson().fromJson(json, type);
        }
        return callLog;
    }

    public static void saveUserData(Context context, UserModel.Data userData) {
        SharedPreferences mPrefs = context.getSharedPreferences(Constants.USER_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString("myJson", new Gson().toJson(userData));
        prefsEditor.apply();
    }

    public static UserModel.Data getUserData(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(Constants.USER_DATA, Context.MODE_PRIVATE);
        String json = mPrefs.getString("myJson", "");
        return new Gson().fromJson(json, UserModel.Data.class);
    }
}
