package com.fastpaper.util.button;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.Button;

import com.fastpaper.util.Constants;

public class ButtonSFDisplayRegular extends Button {

    public ButtonSFDisplayRegular(Context context) {
        super(context);
        applyCustomFont();
    }

    public ButtonSFDisplayRegular(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont();
    }

    public ButtonSFDisplayRegular(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont();
    }

    private void applyCustomFont() {
        if (!TextUtils.isEmpty(Constants.SFDISPLAY_REGULAR)) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), Constants.SFDISPLAY_REGULAR);
            setTypeface(tf);
        }
    }
}
