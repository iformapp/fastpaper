package com.fastpaper.util.edittext;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.EditText;

import com.fastpaper.util.Constants;

public class EditTextSFDisplayBold extends EditText {

    public EditTextSFDisplayBold(Context context) {
        super(context);
        applyCustomFont();
    }

    public EditTextSFDisplayBold(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont();
    }

    public EditTextSFDisplayBold(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont();
    }

    private void applyCustomFont() {
        if (!TextUtils.isEmpty(Constants.SFDISPLAY_BOLD)) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), Constants.SFDISPLAY_BOLD);
            setTypeface(tf);
        }
    }
}
